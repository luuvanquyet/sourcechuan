﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for cls_ChiPhiHoatDong
/// </summary>
public class cls_ChiPhiHoatDong
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public cls_ChiPhiHoatDong()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public bool Linq_Them(string chiphi_content, int chiphi_sotien, string chiphi_month, int nhomhd_id)
    {
        tbChiPhiHoatDong insert = new tbChiPhiHoatDong();
        insert.chiphi_content = chiphi_content;
        insert.chiphi_sotien = chiphi_sotien;
        insert.chiphi_month = chiphi_month;
        insert.nhomhd_id = nhomhd_id;
        insert.hidden = false;
        insert.chiphi_createdate = DateTime.Now;
        db.tbChiPhiHoatDongs.InsertOnSubmit(insert);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int chiphi_id, string chiphi_content, int chiphi_sotien, string chiphi_month, int nhomhd_id)
    {

        tbChiPhiHoatDong update = db.tbChiPhiHoatDongs.Where(x => x.chiphi_id == chiphi_id).FirstOrDefault();
        update.chiphi_content = chiphi_content;
        update.chiphi_sotien = chiphi_sotien;
        update.chiphi_month = chiphi_month;
        update.nhomhd_id = nhomhd_id;
        update.chiphi_update_date = DateTime.Now;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int chiphi_id)
    {
        tbChiPhiHoatDong delete = db.tbChiPhiHoatDongs.Where(x => x.chiphi_id == chiphi_id).FirstOrDefault();
        delete.hidden = true;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}