﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for cls_NhomChiPhiHoatDong
/// </summary>
public class cls_NhomChiPhiHoatDong
{
    dbcsdlDataContext db = new dbcsdlDataContext();
    public cls_NhomChiPhiHoatDong()
    {
        //
        // TODO: Add constructor logic here
        //

    }
    public bool Linq_Them(string nhomhd_name)
    {
        tbNhomChiPhiHoatDong insert = new tbNhomChiPhiHoatDong();
        insert.nhomhd_name = nhomhd_name;
        insert.hidden = false;
        db.tbNhomChiPhiHoatDongs.InsertOnSubmit(insert);
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Sua(int nhomhd_id, string nhomhd_name)
    {

        tbNhomChiPhiHoatDong update = db.tbNhomChiPhiHoatDongs.Where(x => x.nhomhd_id == nhomhd_id).FirstOrDefault();
        update.nhomhd_name = nhomhd_name;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
    public bool Linq_Xoa(int nhomhd_id)
    {
        tbNhomChiPhiHoatDong delete = db.tbNhomChiPhiHoatDongs.Where(x => x.nhomhd_id == nhomhd_id).FirstOrDefault();
        delete.hidden = true;
        try
        {
            db.SubmitChanges();
            return true;
        }
        catch
        {
            return false;
        }
    }
}