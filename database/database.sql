USE [hel13271_Nails]
GO
/****** Object:  User [hel13271_nails]    Script Date: 16/11/2020 3:28:10 PM ******/
CREATE USER [hel13271_nails] FOR LOGIN [hel13271_nails] WITH DEFAULT_SCHEMA=[hel13271_nails]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [hel13271_nails]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [hel13271_nails]
GO
ALTER ROLE [db_datareader] ADD MEMBER [hel13271_nails]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [hel13271_nails]
GO
/****** Object:  Schema [hel13271_nails]    Script Date: 16/11/2020 3:28:10 PM ******/
CREATE SCHEMA [hel13271_nails]
GO
/****** Object:  Table [dbo].[admin_AccessGroupUserForm]    Script Date: 16/11/2020 3:28:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[admin_AccessGroupUserForm](
	[guf_id] [int] IDENTITY(1,1) NOT NULL,
	[guf_active] [bit] NULL,
	[groupuser_id] [int] NULL,
	[form_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[guf_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[admin_AccessGroupUserModule]    Script Date: 16/11/2020 3:28:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[admin_AccessGroupUserModule](
	[gum_id] [int] IDENTITY(1,1) NOT NULL,
	[gum_active] [bit] NULL,
	[groupuser_id] [int] NULL,
	[module_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[gum_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[admin_AccessUserForm]    Script Date: 16/11/2020 3:28:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[admin_AccessUserForm](
	[uf_id] [int] IDENTITY(1,1) NOT NULL,
	[uf_active] [bit] NULL,
	[username_id] [int] NULL,
	[form_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[uf_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[admin_Form]    Script Date: 16/11/2020 3:28:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[admin_Form](
	[form_id] [int] IDENTITY(1,1) NOT NULL,
	[form_position] [int] NULL,
	[form_name] [nvarchar](max) NULL,
	[form_link] [nvarchar](max) NULL,
	[form_active] [bit] NULL,
	[module_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[form_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[admin_GroupUser]    Script Date: 16/11/2020 3:28:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[admin_GroupUser](
	[groupuser_id] [int] IDENTITY(1,1) NOT NULL,
	[groupuser_name] [nvarchar](max) NULL,
	[groupuser_active] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[groupuser_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[admin_Module]    Script Date: 16/11/2020 3:28:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[admin_Module](
	[module_id] [int] IDENTITY(1,1) NOT NULL,
	[module_position] [int] NULL,
	[module_name] [nvarchar](max) NULL,
	[module_icon] [nvarchar](max) NULL,
	[module_active] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[module_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[admin_User]    Script Date: 16/11/2020 3:28:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[admin_User](
	[username_id] [int] IDENTITY(1,1) NOT NULL,
	[username_username] [nvarchar](max) NULL,
	[username_password] [nvarchar](max) NULL,
	[username_fullname] [nvarchar](max) NULL,
	[username_gender] [bit] NULL,
	[username_phone] [nvarchar](max) NULL,
	[username_email] [nvarchar](max) NULL,
	[username_active] [bit] NULL,
	[groupuser_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[username_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbChuongTrinhKhuyenMai]    Script Date: 16/11/2020 3:28:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbChuongTrinhKhuyenMai](
	[khuyenmai_id] [int] IDENTITY(1,1) NOT NULL,
	[khuyenmai_name] [nvarchar](max) NULL,
	[khuyenmai_content] [nvarchar](max) NULL,
	[khuyenmai_percent] [nvarchar](max) NULL,
	[khuyenmai_image] [nvarchar](max) NULL,
	[khuyenmai_tungay] [datetime] NULL,
	[khuyenmai_denngay] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[khuyenmai_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbCity]    Script Date: 16/11/2020 3:28:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbCity](
	[city_id] [int] IDENTITY(1,1) NOT NULL,
	[city_name] [nvarchar](max) NULL,
	[city_position] [int] NULL,
	[city_show] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[city_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbCustomerAccount]    Script Date: 16/11/2020 3:28:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbCustomerAccount](
	[customer_id] [int] IDENTITY(1,1) NOT NULL,
	[customer_firstname] [nvarchar](max) NULL,
	[customer_lastname] [nvarchar](max) NULL,
	[customer_fullname] [nvarchar](max) NULL,
	[customer_phone] [nvarchar](max) NULL,
	[customer_email] [nvarchar](max) NULL,
	[customer_address] [nvarchar](max) NULL,
	[hidden] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[customer_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbDichVu]    Script Date: 16/11/2020 3:28:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbDichVu](
	[dichvu_id] [int] IDENTITY(1,1) NOT NULL,
	[dichvu_title] [nvarchar](max) NULL,
	[dichvu_content] [nvarchar](max) NULL,
	[dichvu_price] [nvarchar](max) NULL,
	[dichvu_image] [nvarchar](max) NULL,
	[dvcate_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[dichvu_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbHoaDonBanHang]    Script Date: 16/11/2020 3:28:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbHoaDonBanHang](
	[hoadon_id] [int] IDENTITY(1,1) NOT NULL,
	[hoadon_code] [nvarchar](max) NULL,
	[khachhang_name] [nvarchar](max) NULL,
	[hoadon_tongtien] [nvarchar](max) NULL,
	[hoadon_giamgia] [nvarchar](max) NULL,
	[hoadon_phaitra] [nvarchar](max) NULL,
	[hoadon_createdate] [datetime] NULL,
	[nhanvien_id] [int] NULL,
	[hoadon_tongtiengiam] [nvarchar](max) NULL,
	[active] [bit] NULL,
	[khachhang_id] [int] NULL,
	[khuyenmai_id] [nvarchar](max) NULL,
	[hoadon_giothanhtoan] [datetime] NULL,
	[hidden] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[hoadon_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbHoaDonBanHangChiTiet]    Script Date: 16/11/2020 3:28:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbHoaDonBanHangChiTiet](
	[hdct_id] [int] IDENTITY(1,1) NOT NULL,
	[hoadon_id] [int] NULL,
	[dichvu_id] [int] NULL,
	[product_id] [int] NULL,
	[hdct_soluong] [int] NULL,
	[hdct_createdate] [datetime] NULL,
	[nhanvien_id] [int] NULL,
	[hdct_price] [nvarchar](max) NULL,
	[hidden] [bit] NULL,
	[hdct_giamgia] [nvarchar](max) NULL,
	[khuyenmai_id] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[hdct_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbIntroduce]    Script Date: 16/11/2020 3:28:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbIntroduce](
	[introduct_id] [int] IDENTITY(1,1) NOT NULL,
	[introduce_title] [nvarchar](max) NULL,
	[introduce_summary] [nvarchar](max) NULL,
	[introduce_content] [nvarchar](max) NULL,
	[introduce_image] [nvarchar](max) NULL,
	[introduce_createdate] [datetime] NULL,
	[introduce_update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[introduct_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbNewCate]    Script Date: 16/11/2020 3:28:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbNewCate](
	[newcate_id] [int] IDENTITY(1,1) NOT NULL,
	[newcate_title] [nvarchar](max) NULL,
	[newcate_summary] [nvarchar](max) NULL,
	[hidden] [bit] NULL,
	[link_seo] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[newcate_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbNews]    Script Date: 16/11/2020 3:28:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbNews](
	[news_id] [int] IDENTITY(1,1) NOT NULL,
	[news_title] [nvarchar](max) NULL,
	[news_summary] [nvarchar](max) NULL,
	[news_image] [nvarchar](max) NULL,
	[news_content] [nvarchar](max) NULL,
	[newcate_id] [int] NULL,
	[hidden] [bit] NULL,
	[active] [bit] NULL,
	[link_seo] [nvarchar](max) NULL,
	[news_createdate] [datetime] NULL,
	[news_position] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[news_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbNhapHang]    Script Date: 16/11/2020 3:28:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbNhapHang](
	[nhaphang_id] [int] IDENTITY(1,1) NOT NULL,
	[nhaphang_code] [nvarchar](max) NULL,
	[nhaphang_createdate] [datetime] NULL,
	[nhaphang_content] [nvarchar](max) NULL,
	[username_id] [int] NULL,
	[hidden] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[nhaphang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbNhapHang_ChiTiet]    Script Date: 16/11/2020 3:28:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbNhapHang_ChiTiet](
	[nhaphang_chitiet_id] [int] IDENTITY(1,1) NOT NULL,
	[nhaphang_id] [int] NULL,
	[product_id] [int] NULL,
	[nhaphang_chitiet_soluong] [int] NULL,
	[nhaphang_code] [nvarchar](max) NULL,
	[nhaphang_gianhap] [int] NULL,
	[nhaphang_thanhtien] [int] NULL,
	[username_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[nhaphang_chitiet_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbProduct]    Script Date: 16/11/2020 3:28:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbProduct](
	[product_id] [int] IDENTITY(1,1) NOT NULL,
	[product_position] [int] NULL,
	[product_title] [nvarchar](max) NULL,
	[product_image] [nvarchar](max) NULL,
	[product_summary] [nvarchar](max) NULL,
	[product_content] [nvarchar](max) NULL,
	[product_quantum] [int] NULL,
	[product_show] [bit] NULL,
	[product_new] [bit] NULL,
	[productcate_id] [int] NULL,
	[title_web] [nvarchar](max) NULL,
	[meta_title] [nvarchar](max) NULL,
	[meta_keywords] [nvarchar](max) NULL,
	[meta_description] [nvarchar](max) NULL,
	[h1_seo] [nvarchar](max) NULL,
	[link_seo] [nvarchar](max) NULL,
	[product_chungloai] [nvarchar](max) NULL,
	[thuonghieu_id] [int] NULL,
	[meta_image] [nvarchar](max) NULL,
	[product_representative] [int] NULL,
	[product_cart] [int] NULL,
	[product_price_new] [int] NULL,
	[product_price] [int] NULL,
	[product_promotions] [int] NULL,
	[product_price_entry] [int] NULL,
	[hidden] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[product_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbProductCate]    Script Date: 16/11/2020 3:28:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbProductCate](
	[productcate_id] [int] IDENTITY(1,1) NOT NULL,
	[productcate_position] [int] NULL,
	[productcate_title] [nvarchar](max) NULL,
	[productcate_show] [bit] NULL,
	[productgroup_id] [int] NULL,
	[title_web] [nvarchar](max) NULL,
	[meta_title] [nvarchar](max) NULL,
	[meta_keywords] [nvarchar](max) NULL,
	[meta_description] [nvarchar](max) NULL,
	[h1_seo] [nvarchar](max) NULL,
	[link_seo] [nvarchar](max) NULL,
	[productcate_parent] [int] NULL,
	[productcate_content] [nvarchar](max) NULL,
	[meta_image] [nvarchar](max) NULL,
	[active] [bit] NULL,
	[hidden] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[productcate_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbSlide]    Script Date: 16/11/2020 3:28:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbSlide](
	[slide_id] [int] IDENTITY(1,1) NOT NULL,
	[slide_image] [nvarchar](max) NULL,
	[slide_title] [nvarchar](max) NULL,
	[slide_title1] [nvarchar](max) NULL,
	[slide_link] [nvarchar](max) NULL,
	[slide_summary] [nvarchar](max) NULL,
	[slide_content] [nvarchar](max) NULL,
	[hidden] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[slide_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbXuatHang]    Script Date: 16/11/2020 3:28:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbXuatHang](
	[xuathang_id] [int] IDENTITY(1,1) NOT NULL,
	[xuathang_code] [nvarchar](max) NULL,
	[xuathang_createdate] [datetime] NULL,
	[xuathang_content] [nvarchar](max) NULL,
	[username_id] [int] NULL,
	[hidden] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[xuathang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbXuatHang_ChiTiet]    Script Date: 16/11/2020 3:28:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbXuatHang_ChiTiet](
	[xuathang_chitiet_id] [int] IDENTITY(1,1) NOT NULL,
	[xuathang_id] [int] NULL,
	[product_id] [int] NULL,
	[xuathang_chitiet_soluong] [int] NULL,
	[username_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[xuathang_chitiet_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [hel13271_nails].[tbNhomDichVu]    Script Date: 16/11/2020 3:28:11 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hel13271_nails].[tbNhomDichVu](
	[dvcate_id] [int] IDENTITY(1,1) NOT NULL,
	[dvcate_name] [nvarchar](max) NULL,
	[position] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[dvcate_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[admin_AccessGroupUserForm] ON 

INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (1, 1, 1, 1)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (2, 1, 1, 2)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (3, 1, 1, 3)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (4, 1, 1, 4)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (5, 1, 1, 5)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (6, 1, 1, 6)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (7, 1, 1, 7)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (8, 1, 1, 8)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (9, 1, 1, 9)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (10, 1, 1, 10)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (11, 1, 1, 11)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (12, 1, 1, 12)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (13, 1, 1, 13)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (14, 1, 1, 14)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (15, 1, 1, 15)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (16, 1, 1, 16)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (17, 1, 2, 4)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (18, 1, 2, 5)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (19, 1, 2, 6)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (20, 1, 2, 8)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (21, 1, 2, 9)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (22, 1, 2, 10)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (23, 1, 2, 11)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (24, 1, 2, 12)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (25, 1, 2, 15)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (26, 1, 2, 16)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (27, 1, 2, 13)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (28, 1, 2, 14)
SET IDENTITY_INSERT [dbo].[admin_AccessGroupUserForm] OFF
SET IDENTITY_INSERT [dbo].[admin_AccessGroupUserModule] ON 

INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (1, 1, 1, 1)
INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (2, 1, 1, 2)
INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (3, 1, 1, 3)
INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (4, 1, 1, 4)
INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (5, 1, 1, 5)
INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (6, 1, 1, 6)
INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (7, 1, 1, 7)
INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (8, 1, 2, 2)
INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (9, 1, 2, 3)
INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (10, 1, 2, 4)
INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (11, 1, 2, 5)
INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (12, 1, 2, 6)
INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (13, 1, 2, 7)
SET IDENTITY_INSERT [dbo].[admin_AccessGroupUserModule] OFF
SET IDENTITY_INSERT [dbo].[admin_AccessUserForm] ON 

INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (1, 1, 1, 1)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (2, 1, 1, 2)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (3, 1, 1, 3)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (4, 1, 1, 4)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (5, 1, 1, 5)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (6, 1, 1, 6)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (7, 1, 1, 7)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (8, 1, 1, 8)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (9, 1, 1, 9)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (10, 1, 1, 10)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (11, 1, 1, 11)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (12, 1, 1, 12)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (13, 1, 1, 13)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (14, 1, 1, 14)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (15, 1, 1, 15)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (16, 1, 1, 16)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (17, 1, 2, 4)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (18, 1, 2, 5)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (19, 1, 2, 6)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (20, 1, 2, 8)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (21, 1, 2, 9)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (22, 1, 2, 10)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (23, 1, 2, 11)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (24, 1, 2, 12)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (25, 1, 2, 15)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (26, 1, 2, 16)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (27, 1, 2, 13)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (28, 1, 2, 14)
SET IDENTITY_INSERT [dbo].[admin_AccessUserForm] OFF
SET IDENTITY_INSERT [dbo].[admin_Form] ON 

INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (1, 1, N'Quản lý phân quyền', N'admin-access', 1, 1)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (2, 2, N'Quản lý Module', N'admin-module', 1, 1)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (3, 3, N'Quản lý Form', N'admin-form', 1, 1)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (4, 1, N'Quản lý tài khoản', N'admin-account', 1, 2)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (5, 1, N'Quản lý nhóm sản phẩm', N'admin-quan-ly-nhom-san-pham', 1, 3)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (6, 2, N'Quản lý sản phẩm', N'admin-quan-ly-san-pham', 1, 3)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (7, 3, N'Quản lý nhóm tin tức', N'admin-new-cate', 0, 4)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (8, 4, N'Quản lý tin tức', N'admin-news', 1, 4)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (9, 2, N'Quản lý slide', N'admin-slide', 1, 4)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (10, 1, N'Quản lý giới thiệu', N'admin-introduce', 1, 4)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (11, 5, N'Chương trình khuyến mãi', N'admin-chuong-trinh-khuyen-mai', 1, 4)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (12, 6, N'Tài khoản khách hàng', N'admin-tai-khoan-khach-hang', 1, 4)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (13, 1, N'Quản lý dịch vụ', N'admin-quan-ly-dich-vu', 1, 6)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (14, 1, N'Hóa đơn bán hàng', N'admin-hoa-don-ban-hang', 1, 7)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (15, 1, N'Quản lý nhập hàng', N'admin-quan-ly-nhap-hang', 1, 5)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (16, 2, N'Quản lý xuất hàng', N'admin-quan-ly-xuat-hang', 1, 5)
SET IDENTITY_INSERT [dbo].[admin_Form] OFF
SET IDENTITY_INSERT [dbo].[admin_GroupUser] ON 

INSERT [dbo].[admin_GroupUser] ([groupuser_id], [groupuser_name], [groupuser_active]) VALUES (1, N'root', 1)
INSERT [dbo].[admin_GroupUser] ([groupuser_id], [groupuser_name], [groupuser_active]) VALUES (2, N'Admin', 1)
INSERT [dbo].[admin_GroupUser] ([groupuser_id], [groupuser_name], [groupuser_active]) VALUES (3, N'Nhân viên', 1)
SET IDENTITY_INSERT [dbo].[admin_GroupUser] OFF
SET IDENTITY_INSERT [dbo].[admin_Module] ON 

INSERT [dbo].[admin_Module] ([module_id], [module_position], [module_name], [module_icon], [module_active]) VALUES (1, 1, N'Phân quyền', N'', 1)
INSERT [dbo].[admin_Module] ([module_id], [module_position], [module_name], [module_icon], [module_active]) VALUES (2, 2, N'Tài khoản', N'fas fa-users', 1)
INSERT [dbo].[admin_Module] ([module_id], [module_position], [module_name], [module_icon], [module_active]) VALUES (3, 4, N'Quản lý sản phẩm', N'', 1)
INSERT [dbo].[admin_Module] ([module_id], [module_position], [module_name], [module_icon], [module_active]) VALUES (4, 3, N'Quản lý website', N'', 1)
INSERT [dbo].[admin_Module] ([module_id], [module_position], [module_name], [module_icon], [module_active]) VALUES (5, 5, N'Quản lý Nhập-Xuất hàng', N'', 1)
INSERT [dbo].[admin_Module] ([module_id], [module_position], [module_name], [module_icon], [module_active]) VALUES (6, 6, N'Quản lý dịch vụ', N'', 1)
INSERT [dbo].[admin_Module] ([module_id], [module_position], [module_name], [module_icon], [module_active]) VALUES (7, 7, N'Hóa đơn', NULL, 1)
SET IDENTITY_INSERT [dbo].[admin_Module] OFF
SET IDENTITY_INSERT [dbo].[admin_User] ON 

INSERT [dbo].[admin_User] ([username_id], [username_username], [username_password], [username_fullname], [username_gender], [username_phone], [username_email], [username_active], [groupuser_id]) VALUES (1, N'root', N'12378248145104161527610811213823414203124130', N'ROOT', 1, N'01634057167', N'qutienpro@gmail.com', 1, 1)
INSERT [dbo].[admin_User] ([username_id], [username_username], [username_password], [username_fullname], [username_gender], [username_phone], [username_email], [username_active], [groupuser_id]) VALUES (2, N'admin', N'12378248145104161527610811213823414203124130', N'Quản trị', 1, N'12345', N'quantri@hifiveplus.vn', 1, 2)
INSERT [dbo].[admin_User] ([username_id], [username_username], [username_password], [username_fullname], [username_gender], [username_phone], [username_email], [username_active], [groupuser_id]) VALUES (3, N'staff', N'12378248145104161527610811213823414203124130', N'Nhân viên', 0, N'67891', N'staff@hifiveplus.vn', 0, 3)
INSERT [dbo].[admin_User] ([username_id], [username_username], [username_password], [username_fullname], [username_gender], [username_phone], [username_email], [username_active], [groupuser_id]) VALUES (4, N'quyetlv', N'12345', N'Lưu Văn Quyết', NULL, N'0334798366', N'quyet@gmail.com', 1, 3)
INSERT [dbo].[admin_User] ([username_id], [username_username], [username_password], [username_fullname], [username_gender], [username_phone], [username_email], [username_active], [groupuser_id]) VALUES (5, N'hoanglan', N'123456', N'Hoàng Lan', NULL, N'0919698094', N'hoanglan6192@gmail.com', 1, 3)
SET IDENTITY_INSERT [dbo].[admin_User] OFF
SET IDENTITY_INSERT [dbo].[tbChuongTrinhKhuyenMai] ON 

INSERT [dbo].[tbChuongTrinhKhuyenMai] ([khuyenmai_id], [khuyenmai_name], [khuyenmai_content], [khuyenmai_percent], [khuyenmai_image], [khuyenmai_tungay], [khuyenmai_denngay]) VALUES (1, N'Khuyến mãi cuối năm', N'Giảm giá 5% tất cả các sản phẩm tại cửa hàng, bạn cũng có thể ngoại trừ một số mặt hàng đặc trưng, hình thức này luôn được đánh giá là thu hút được nhiều sự chú ý và mang lại nhiều doanh thu hơn cho các cửa hàng bán lẻ.', N'10', N'/admin_images/up-img.png', CAST(N'2020-11-16 00:00:00.000' AS DateTime), CAST(N'2020-11-22 00:00:00.000' AS DateTime))
INSERT [dbo].[tbChuongTrinhKhuyenMai] ([khuyenmai_id], [khuyenmai_name], [khuyenmai_content], [khuyenmai_percent], [khuyenmai_image], [khuyenmai_tungay], [khuyenmai_denngay]) VALUES (2, N'Khuyến mãi khai trương', N'Nội dung khuyến mãi', N'20', N'/uploadimages/anh_khuyenmai/14112020_013116_CH_z2159486269357_fed878df4bb9a2d8c59f0dd58dadfb3c.jpg', CAST(N'2020-11-16 00:00:00.000' AS DateTime), CAST(N'2020-11-22 00:00:00.000' AS DateTime))
INSERT [dbo].[tbChuongTrinhKhuyenMai] ([khuyenmai_id], [khuyenmai_name], [khuyenmai_content], [khuyenmai_percent], [khuyenmai_image], [khuyenmai_tungay], [khuyenmai_denngay]) VALUES (3, N'Khuyến mãi', N'', N'5', N'/admin_images/up-img.png', CAST(N'2020-11-10 00:00:00.000' AS DateTime), CAST(N'2020-11-13 00:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[tbChuongTrinhKhuyenMai] OFF
SET IDENTITY_INSERT [dbo].[tbCity] ON 

INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (1, N'An Giang', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (2, N'Bà Rịa - Vũng Tàu
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (3, N'Bắc Giang
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (4, N'Bắc Kạn
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (5, N'Bạc Liêu
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (6, N'Bắc Ninh
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (7, N'Bến Tre
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (8, N'Bình Định
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (9, N'Bình Dương
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (10, N'Bình Phước
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (11, N'Bình Thuận
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (12, N'Cà Mau
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (13, N'Cao Bằng
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (14, N'Đắk Lắk
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (15, N'Đắk Nông
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (16, N'Điện Biên
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (17, N'Đồng Nai
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (18, N'Đồng Tháp
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (19, N'Gia Lai
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (20, N'Hà Giang
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (21, N'Hà Nam
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (22, N'Hà Tĩnh
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (23, N'Hải Dương
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (24, N'Hậu Giang
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (25, N'Hòa Bình
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (26, N'Hưng Yên
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (27, N'Khánh Hòa
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (28, N'Kiên Giang
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (29, N'Kon Tum
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (30, N'Lai Châu
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (31, N'Lâm Đồng
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (32, N'Lạng Sơn
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (33, N'Lào Cai
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (34, N'Long An
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (35, N'Nam Định
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (36, N'Nghệ An
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (37, N'Ninh Bình
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (38, N'Ninh Thuận
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (39, N'Phú Thọ
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (40, N'Quảng Bình
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (41, N'Quảng Nam
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (42, N'Quảng Ngãi
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (43, N'Quảng Ninh
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (44, N'Quảng Trị
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (45, N'Sóc Trăng
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (46, N'Sơn La
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (47, N'Tây Ninh
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (48, N'Thái Bình
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (49, N'Thái Nguyên
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (50, N'Thanh Hóa
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (51, N'Thừa Thiên Huế
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (52, N'Tiền Giang
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (53, N'Trà Vinh
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (54, N'Tuyên Quang
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (55, N'Vĩnh Long
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (56, N'Vĩnh Phúc
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (57, N'Yên Bái
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (58, N'Phú Yên
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (59, N'Cần Thơ
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (60, N'Đà Nẵng
', 1, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (61, N'Hải Phòng
', 2, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (62, N'Hà Nội
', 3, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (63, N'TP HCM
', 4, NULL)
SET IDENTITY_INSERT [dbo].[tbCity] OFF
SET IDENTITY_INSERT [dbo].[tbCustomerAccount] ON 

INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (1, NULL, NULL, N'Khách hàng 1', N'1', NULL, N'Đà Nẵng', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (2, NULL, NULL, N'Khách hàng 2', N'2', NULL, N'Đà Nẵng', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (3, NULL, NULL, N'Khách hàng 3', N'3', NULL, N'Đà Nẵng', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (4, NULL, NULL, N'Khách hàng 4', N'4', NULL, N'Đà nẵng', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (5, NULL, NULL, N'Khách hàng 5', N'5', NULL, N'Đà Nẵng', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (6, NULL, NULL, N'Khách hàng 6', N'6', NULL, N'Đà Nẵng', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (7, NULL, NULL, N'Khách hàng 7', N'7', NULL, N'Đà Nẵng', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (8, NULL, NULL, N'Khách hàng 8', N'8', NULL, N'Đà Nẵng', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (9, NULL, NULL, N'Khách hàng 9', N'9', NULL, N'Đà Nẵng', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (10, NULL, NULL, N'Khách hàng 10', N'10', NULL, N'Đà Nẵng', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (11, NULL, NULL, N'Kim thoa', N'123', NULL, N'', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (12, NULL, NULL, N'Kim chỉ', N'234', NULL, N'', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (13, NULL, NULL, N'Kim Tú', N'0905912552', NULL, N'', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (14, NULL, NULL, N'kim châm', N'12', NULL, N'', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (15, NULL, NULL, N'Kim trọng', N'321', NULL, N'', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (16, NULL, NULL, N'Khánh Chi', N'0905339033', NULL, N'', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (17, NULL, NULL, N'Duyên', N'0966176134', NULL, N'', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (18, NULL, NULL, N'Kiều Thuận', N'0905439392', NULL, N'', 0)
SET IDENTITY_INSERT [dbo].[tbCustomerAccount] OFF
SET IDENTITY_INSERT [dbo].[tbDichVu] ON 

INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (1, N'Tẩy da chết chân 30k', N'', N'30000', N'/admin_images/up-img.png', 1)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (2, N'Chà gót chân 50k', N'', N'50000', N'/admin_images/up-img.png', 1)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (3, N'Phá Gel 30k', N'', N'30000', N'/uploadimages/anh_dichvu/14112020_104850_SA_your icon.png', 1)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (4, N'Sửa móng 20k', N'Lấy khóe, nhặt da, chỉnh form móng, dũa máy', N'20000', N'/uploadimages/anh_dichvu/14112020_104715_SA_messager.png', 1)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (5, N'Ngâm chân thảo dược Massega 80k', N'', N'80000', N'/admin_images/up-img.png', 1)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (6, N'Gội đầu thường 20k', N'', N'20000', N'/admin_images/up-img.png', 3)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (7, N'Gội đầu cập 50k', N'', N'50000', N'/admin_images/up-img.png', 3)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (8, N'Rửa mặt tẩy da chết 40k', N'', N'40000', N'/admin_images/up-img.png', 3)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (9, N'Sơn bóng móng 25k', N'', N'25000', N'/admin_images/up-img.png', 5)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (10, N'Sơn thường 30k', N'', N'30000', N'/admin_images/up-img.png', 5)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (11, N'Sơn Gel 59k', N'', N'59000', N'/admin_images/up-img.png', 5)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (12, N'Sơn cứng móng 15k', N'', N'15000', N'/admin_images/up-img.png', 5)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (13, N'Sơn nhũ 60k', N'', N'60000', N'/admin_images/up-img.png', 5)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (14, N'Sơn mắt mèo 70k', N'', N'70000', N'/admin_images/up-img.png', 5)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (15, N'Dán móng giả 60k', N'', N'60000', N'/admin_images/up-img.png', 6)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (16, N'Đắp Gel 150k', N'', N'150000', N'/admin_images/up-img.png', 6)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (17, N'Đắp bột 180k', N'', N'180000', N'/admin_images/up-img.png', 6)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (18, N'Móng úp 80k', N'', N'80000', N'/admin_images/up-img.png', 6)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (19, N'Đá nhô (1k)', N'', N'1000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (20, N'Đá nhô (2k)', N'', N'2000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (21, N'Đá nhô (3k)', N'', N'3000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (22, N'Đá nhô (4k)', N'', N'4000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (23, N'Đá nhô (5k)', N'', N'5000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (24, N'Chàm nhỏ (1k)', N'', N'1000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (25, N'Chàm nhỏ (2k)', N'', N'2000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (26, N'Chàm nhỏ (3k)', N'', N'3000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (27, N'Chàm nhỏ (4k)', N'', N'4000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (28, N'Chàm nhỏ (5k)', N'', N'5000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (29, N'Đá hình 15k', N'', N'15000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (30, N'Đá hình 10k', N'', N'10000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (31, N'Đá hình 20k', N'', N'20000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (32, N'Đá khối/đá hình siêu sáng 10k', N'', N'10000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (33, N'Đá khối/đá hình siêu sáng 20k', N'', N'20000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (34, N'Đá khối/đá hình siêu sáng 30k', N'', N'30000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (35, N'Đá khối/đá hình siêu sáng 40k', N'', N'40000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (36, N'Charm 10k', N'', N'10000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (37, N'Charm 20k', N'', N'20000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (38, N'Charm 30k', N'', N'30000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (39, N'Hoa bột 10k', N'', N'10000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (40, N'Hoa bột 20k', N'', N'20000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (41, N'Hoa bột 30k', N'', N'30000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (43, N'Onbre 5k/ngón', N'', N'5000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (44, N'Onbre 10k/ngón', N'', N'10000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (45, N'Vân đá', N'', N'10000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (46, N'Kẻ đầu móng 5k', N'', N'5000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (47, N'Kẻ đầu móng 10k', N'', N'10000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (48, N'Ẩn hoa 10k', N'', N'10000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (49, N'Ẩn hoa 20k', N'', N'20000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (50, N'Xà cừ 10k', N'', N'10000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (51, N'Xà cừ 20k', N'', N'20000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (52, N'Ẩn nhũ 10k', N'', N'10000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (53, N'Ẩn nhũ 20k', N'', N'20000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (54, N'Loang 10k', N'', N'10000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (55, N'Loang 20k', N'', N'20000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (56, N'Dập phôi', N'', N'5000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (57, N'Tráng gương', N'', N'5000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (58, N'Mắt mèo', N'', N'10000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (59, N'Vẽ Gel (họa tiết, design...) 5k', N'', N'5000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (60, N'Vẽ Gel (họa tiết, design...) 10k', N'', N'10000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (61, N'Vẽ Gel (họa tiết, design...) 15k', N'', N'15000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (62, N'Vẽ Gel (họa tiết, design...) 20k', N'', N'20000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (63, N'Vẽ Gel (họa tiết, design...) 25k', N'', N'25000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (64, N'Vẽ Gel (họa tiết, design...) 30k', N'', N'30000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (65, N'Vẽ Gel (họa tiết, design...) 35k', N'', N'35000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (66, N'Đắp nhũ 10k', N'', N'10000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (67, N'Đắp nhũ 20k', N'', N'20000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (68, N'Phụ kiện (sticker,hình dán nước...) 10k', N'', N'10000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (69, N'Phụ kiện (sticker,hình dán nước...) 20k', N'', N'20000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (70, N'Phá Gel 10k', N'', N'10000', NULL, 1)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (71, N'Phá Gel 20k', N'', N'20000', NULL, 1)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (72, N'Rửa mặt 15k', N'', N'15000', NULL, 3)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (73, N'Sấy tạo kiểu 20k', N'', N'20000', NULL, 3)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (74, N'Tạo kiểu 10k', N'', N'10000', NULL, 3)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (75, N'Nhũ 5k', N'', N'5000', NULL, 2)
SET IDENTITY_INSERT [dbo].[tbDichVu] OFF
SET IDENTITY_INSERT [dbo].[tbHoaDonBanHang] ON 

INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [nhanvien_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden]) VALUES (37, N'HD00007', N'khách hàng a', NULL, N'20', NULL, CAST(N'2020-11-14 16:50:14.697' AS DateTime), 1, NULL, 1, NULL, N'2', NULL, NULL)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [nhanvien_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden]) VALUES (38, N'HD00008', N'bhasgd', NULL, NULL, NULL, CAST(N'2020-11-14 16:52:04.043' AS DateTime), 1, NULL, 1, 15, NULL, NULL, NULL)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [nhanvien_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden]) VALUES (47, N'HD00009', N'Kim Tú', N'250000', N'20', N'200000', CAST(N'2020-11-14 18:20:23.887' AS DateTime), 2, N'50000', 1, NULL, N'2', CAST(N'2020-11-14 18:20:54.513' AS DateTime), 0)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [nhanvien_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden]) VALUES (48, N'HD00010', N'Kim Tú', N'290000', N'20', N'232000', CAST(N'2020-11-14 18:22:44.483' AS DateTime), 2, N'58000', 1, NULL, N'2', CAST(N'2020-11-14 18:23:19.390' AS DateTime), 0)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [nhanvien_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden]) VALUES (49, N'HD00011', N'Khách hàng 1', N'100000', N'10', N'90000', CAST(N'2020-11-14 19:07:25.153' AS DateTime), 1, N'10000', 0, 1, N'1', CAST(N'2020-11-14 19:07:58.783' AS DateTime), 1)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [nhanvien_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden]) VALUES (50, N'HD00012', N'0919698094', N'40000', N'0', N'40000', CAST(N'2020-11-15 14:49:28.153' AS DateTime), 1, N'0', 0, NULL, N'', CAST(N'2020-11-15 14:51:36.767' AS DateTime), 1)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [nhanvien_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden]) VALUES (51, N'HD00013', N'0919698094', N'13000', N'0', N'13000', CAST(N'2020-11-15 14:54:04.050' AS DateTime), 2, N'0', 0, NULL, N'', CAST(N'2020-11-15 14:55:01.597' AS DateTime), 1)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [nhanvien_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden]) VALUES (52, N'HD00014', N'0919698094', N'30000', N'10', N'27000', CAST(N'2020-11-15 14:56:49.147' AS DateTime), 1, N'3000', 0, NULL, N'1', CAST(N'2020-11-15 15:27:10.653' AS DateTime), 1)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [nhanvien_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden]) VALUES (53, N'HD00015', N'acs', N'30000', N'0', N'30000', CAST(N'2020-11-15 15:30:35.890' AS DateTime), 2, N'0', 0, NULL, N'', CAST(N'2020-11-15 15:30:39.560' AS DateTime), 1)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [nhanvien_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden]) VALUES (54, N'HD00016', N'Kim Tú', N'310000', N'0', N'310000', CAST(N'2020-11-15 15:47:59.237' AS DateTime), 2, N'0', 0, NULL, N'', CAST(N'2020-11-15 15:48:03.737' AS DateTime), 1)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [nhanvien_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden]) VALUES (55, N'HD00017', N'Kim Tú', N'120000', N'10', N'108000', CAST(N'2020-11-15 16:02:17.773' AS DateTime), 2, N'12000', 0, NULL, N'1', CAST(N'2020-11-15 16:02:40.037' AS DateTime), 1)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [nhanvien_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden]) VALUES (56, N'HD00018', N'0919698094', N'20000', N'0', N'20000', CAST(N'2020-11-15 16:04:43.010' AS DateTime), 2, N'0', 0, NULL, N'', CAST(N'2020-11-15 16:04:55.510' AS DateTime), 1)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [nhanvien_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden]) VALUES (58, N'HD00019', N'Khánh Chi', N'121000', N'20', N'96800', CAST(N'2020-11-15 16:07:44.137' AS DateTime), 2, N'24200', 0, 16, N'2', CAST(N'2020-11-15 16:11:01.657' AS DateTime), 1)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [nhanvien_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden]) VALUES (59, N'HD00020', N'Khánh Chi', NULL, N'20', NULL, CAST(N'2020-11-15 16:15:45.257' AS DateTime), 2, NULL, 1, NULL, N'2', NULL, NULL)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [nhanvien_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden]) VALUES (61, N'HD00021', N'Khánh Chi', N'168000', N'0', N'168000', CAST(N'2020-11-16 08:59:32.163' AS DateTime), 2, N'0', 0, NULL, N'', CAST(N'2020-11-16 12:25:45.063' AS DateTime), 1)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [nhanvien_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden]) VALUES (64, N'HD00023', N'Khách hàng 1', N'35000', N'20', N'28000', CAST(N'2020-11-16 09:21:28.533' AS DateTime), 2, N'7000', 0, 1, N'2', CAST(N'2020-11-16 09:21:36.610' AS DateTime), 0)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [nhanvien_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden]) VALUES (66, N'HD00025', N'Khách hàng 1', N'60000', N'20', N'48000', CAST(N'2020-11-16 09:59:28.120' AS DateTime), 2, N'12000', 0, 1, N'2', CAST(N'2020-11-16 10:12:08.633' AS DateTime), 0)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [nhanvien_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden]) VALUES (70, N'HD00029', N'Kiều Thuận', N'196000', N'20', N'156800', CAST(N'2020-11-16 11:48:40.627' AS DateTime), 2, N'39200', 0, 18, N'2', CAST(N'2020-11-16 11:52:08.180' AS DateTime), 0)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [nhanvien_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden]) VALUES (71, N'HD00030', N'Khánh Chi', N'168000', N'20', N'134400', CAST(N'2020-11-16 12:27:54.487' AS DateTime), 2, N'33600', 0, NULL, N'2', CAST(N'2020-11-16 12:28:05.380' AS DateTime), 0)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [nhanvien_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden]) VALUES (72, N'HD00031', N'Lân', N'113000', N'20', N'90400', CAST(N'2020-11-16 12:29:55.130' AS DateTime), 2, N'22600', 0, NULL, N'2', CAST(N'2020-11-16 12:30:06.397' AS DateTime), 0)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [nhanvien_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden]) VALUES (74, N'HD00033', N'Khách hàng 2', N'99000', N'20', N'79200', CAST(N'2020-11-16 12:52:40.960' AS DateTime), 2, N'19800', 0, 2, N'2', CAST(N'2020-11-16 12:52:47.287' AS DateTime), 0)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [nhanvien_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden]) VALUES (75, N'HD00034', N'Khách hàng 1', NULL, N'20', NULL, CAST(N'2020-11-16 13:07:16.587' AS DateTime), 2, NULL, 1, 1, N'2', NULL, NULL)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [nhanvien_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden]) VALUES (76, N'HD00035', N'Khách hàng 2', N'326000', N'20', N'260800', CAST(N'2020-11-16 13:59:49.130' AS DateTime), 2, N'65200', 0, 2, N'2', CAST(N'2020-11-16 15:04:43.490' AS DateTime), 0)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [nhanvien_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden]) VALUES (78, N'HD00037', N'Khách hàng 4', NULL, N'20', NULL, CAST(N'2020-11-16 14:14:42.837' AS DateTime), 2, NULL, 1, 4, N'2', NULL, NULL)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [nhanvien_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden]) VALUES (79, N'HD00038', N'Khách hàng 5', NULL, N'20', NULL, CAST(N'2020-11-16 14:27:14.303' AS DateTime), 2, NULL, 1, 5, N'2', NULL, NULL)
SET IDENTITY_INSERT [dbo].[tbHoaDonBanHang] OFF
SET IDENTITY_INSERT [dbo].[tbHoaDonBanHangChiTiet] ON 

INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (78, 28, 4, NULL, 0, CAST(N'2020-11-14 13:54:32.560' AS DateTime), 2, N'200000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (79, 28, 1, NULL, 0, CAST(N'2020-11-14 13:54:32.560' AS DateTime), 2, N'150000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (80, 28, 2, NULL, 7, CAST(N'2020-11-14 13:54:32.560' AS DateTime), 2, N'200000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (81, 28, 3, NULL, 2, CAST(N'2020-11-14 13:54:32.560' AS DateTime), 2, N'180000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (82, 24, 1, NULL, 2, CAST(N'2020-11-14 13:56:36.550' AS DateTime), 1, N'150000', 0, N'10', N'1')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (83, 24, 2, NULL, 1, CAST(N'2020-11-14 13:56:36.647' AS DateTime), 1, N'200000', 0, N'10', N'1')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (84, 24, 4, NULL, 1, CAST(N'2020-11-14 13:56:36.697' AS DateTime), 1, N'200000', 0, N'10', N'1')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (87, 29, 4, NULL, 1, CAST(N'2020-11-14 13:57:26.140' AS DateTime), 2, N'200000', 0, N'0', N'')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (88, 29, 2, NULL, 1, CAST(N'2020-11-14 13:57:26.140' AS DateTime), 2, N'200000', 0, N'0', N'')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (91, 30, 4, NULL, 1, CAST(N'2020-11-14 14:06:51.713' AS DateTime), 2, N'200000', 0, NULL, NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (92, 30, 2, NULL, 1, CAST(N'2020-11-14 14:06:51.713' AS DateTime), 2, N'200000', 0, NULL, NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (97, 31, 4, NULL, 2, CAST(N'2020-11-14 14:07:53.607' AS DateTime), 2, N'200000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (98, 31, 3, NULL, 2, CAST(N'2020-11-14 14:07:53.607' AS DateTime), 2, N'180000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (99, 31, 2, NULL, 2, CAST(N'2020-11-14 14:07:53.607' AS DateTime), 2, N'200000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (103, 37, 7, NULL, 2, CAST(N'2020-11-14 16:50:39.083' AS DateTime), 1, N'50000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (104, 37, 14, NULL, 2, CAST(N'2020-11-14 16:50:46.990' AS DateTime), 1, N'70000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (107, 32, 1, NULL, 3, CAST(N'2020-11-14 16:54:50.083' AS DateTime), 2, N'300000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (108, 32, 3, NULL, 3, CAST(N'2020-11-14 16:54:50.100' AS DateTime), 2, N'30000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (109, 32, 2, NULL, 2, CAST(N'2020-11-14 16:54:50.100' AS DateTime), 2, N'500000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (125, 47, 6, NULL, 1, CAST(N'2020-11-14 18:20:54.497' AS DateTime), 2, N'20000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (126, 47, 17, NULL, 1, CAST(N'2020-11-14 18:20:54.497' AS DateTime), 2, N'180000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (127, 47, 49, NULL, 1, CAST(N'2020-11-14 18:20:54.497' AS DateTime), 2, N'20000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (128, 47, 21, NULL, 10, CAST(N'2020-11-14 18:20:54.513' AS DateTime), 2, N'3000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (133, 48, 7, NULL, 1, CAST(N'2020-11-14 18:23:19.390' AS DateTime), 2, N'50000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (134, 48, 17, NULL, 1, CAST(N'2020-11-14 18:23:19.390' AS DateTime), 2, N'180000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (135, 48, 69, NULL, 1, CAST(N'2020-11-14 18:23:19.390' AS DateTime), 2, N'20000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (136, 48, 22, NULL, 10, CAST(N'2020-11-14 18:23:19.390' AS DateTime), 2, N'4000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (139, 49, 71, NULL, 2, CAST(N'2020-11-14 19:07:58.673' AS DateTime), 1, N'20000', 0, N'10', N'1')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (140, 49, 3, NULL, 2, CAST(N'2020-11-14 19:07:58.727' AS DateTime), 1, N'30000', 0, N'10', N'1')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (142, 50, 8, NULL, 1, CAST(N'2020-11-15 14:51:36.750' AS DateTime), 1, N'40000', 0, N'0', N'')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (145, 51, 26, NULL, 1, CAST(N'2020-11-15 14:55:01.567' AS DateTime), 2, N'3000', 0, N'0', N'')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (146, 51, 32, NULL, 1, CAST(N'2020-11-15 14:55:01.580' AS DateTime), 2, N'10000', 0, N'0', N'')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (149, 52, 48, NULL, 1, CAST(N'2020-11-15 15:27:10.637' AS DateTime), 2, N'10000', 0, N'10', N'1')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (150, 52, 53, NULL, 1, CAST(N'2020-11-15 15:27:10.637' AS DateTime), 2, N'20000', 0, N'10', N'1')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (153, 53, 70, NULL, 1, CAST(N'2020-11-15 15:30:39.560' AS DateTime), 2, N'10000', 0, N'0', N'')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (154, 53, 71, NULL, 1, CAST(N'2020-11-15 15:30:39.560' AS DateTime), 2, N'20000', 0, N'0', N'')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (157, 54, 70, NULL, 1, CAST(N'2020-11-15 15:48:03.723' AS DateTime), 2, N'10000', 0, N'0', N'')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (158, 54, 1, NULL, 1, CAST(N'2020-11-15 15:48:03.723' AS DateTime), 2, N'300000', 0, N'0', N'')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (162, 55, 70, NULL, 2, CAST(N'2020-11-15 16:02:40.037' AS DateTime), 2, N'10000', 0, N'10', N'1')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (163, 55, 71, NULL, 2, CAST(N'2020-11-15 16:02:40.037' AS DateTime), 2, N'20000', 0, N'10', N'1')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (164, 55, 3, NULL, 2, CAST(N'2020-11-15 16:02:40.037' AS DateTime), 2, N'30000', 0, N'10', N'1')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (166, 56, 71, NULL, 1, CAST(N'2020-11-15 16:04:55.510' AS DateTime), 2, N'20000', 0, N'0', N'')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (171, 58, 4, NULL, 2, CAST(N'2020-11-15 16:11:01.610' AS DateTime), 2, N'20000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (172, 58, 19, NULL, 2, CAST(N'2020-11-15 16:11:01.623' AS DateTime), 2, N'1000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (173, 58, 58, NULL, 2, CAST(N'2020-11-15 16:11:01.640' AS DateTime), 2, N'10000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (174, 58, 11, NULL, 1, CAST(N'2020-11-15 16:11:01.640' AS DateTime), 2, N'59000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (175, 59, 4, NULL, 2, CAST(N'2020-11-15 16:15:54.367' AS DateTime), 2, N'20000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (176, 59, 11, NULL, 2, CAST(N'2020-11-15 16:16:03.837' AS DateTime), 2, N'59000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (177, 59, 19, NULL, 2, CAST(N'2020-11-15 16:16:14.820' AS DateTime), 2, N'1000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (178, 59, 58, NULL, 2, CAST(N'2020-11-15 16:17:20.103' AS DateTime), 2, N'10000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (184, 64, 72, NULL, 1, CAST(N'2020-11-16 09:21:36.610' AS DateTime), 2, N'15000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (185, 64, 6, NULL, 1, CAST(N'2020-11-16 09:21:36.610' AS DateTime), 2, N'20000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (193, 66, 7, NULL, 1, CAST(N'2020-11-16 10:12:08.633' AS DateTime), 2, N'50000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (194, 66, 74, NULL, 1, CAST(N'2020-11-16 10:12:08.633' AS DateTime), 2, N'10000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (208, 70, 19, NULL, 8, CAST(N'2020-11-16 11:52:08.163' AS DateTime), 2, N'1000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (209, 70, 4, NULL, 2, CAST(N'2020-11-16 11:52:08.163' AS DateTime), 2, N'20000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (210, 70, 11, NULL, 2, CAST(N'2020-11-16 11:52:08.180' AS DateTime), 2, N'59000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (211, 70, 75, NULL, 2, CAST(N'2020-11-16 11:52:08.180' AS DateTime), 2, N'5000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (212, 70, 48, NULL, 2, CAST(N'2020-11-16 11:52:08.180' AS DateTime), 2, N'10000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (213, 61, 11, NULL, 2, CAST(N'2020-11-16 12:25:45.047' AS DateTime), 2, N'59000', 0, N'0', N'')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (214, 61, 4, NULL, 2, CAST(N'2020-11-16 12:25:45.047' AS DateTime), 2, N'20000', 0, N'0', N'')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (215, 61, 19, NULL, 10, CAST(N'2020-11-16 12:25:45.047' AS DateTime), 2, N'1000', 0, N'0', N'')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (219, 71, 4, NULL, 2, CAST(N'2020-11-16 12:28:05.363' AS DateTime), 2, N'20000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (220, 71, 11, NULL, 2, CAST(N'2020-11-16 12:28:05.363' AS DateTime), 2, N'59000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (221, 71, 19, NULL, 10, CAST(N'2020-11-16 12:28:05.363' AS DateTime), 2, N'1000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (226, 72, 6, NULL, 1, CAST(N'2020-11-16 12:30:06.380' AS DateTime), 2, N'20000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (227, 72, 11, NULL, 1, CAST(N'2020-11-16 12:30:06.380' AS DateTime), 2, N'59000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (228, 72, 20, NULL, 7, CAST(N'2020-11-16 12:30:06.380' AS DateTime), 2, N'2000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (229, 72, 60, NULL, 2, CAST(N'2020-11-16 12:30:06.397' AS DateTime), 2, N'10000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (237, 74, 11, NULL, 1, CAST(N'2020-11-16 12:52:47.287' AS DateTime), 2, N'59000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (238, 74, 4, NULL, 1, CAST(N'2020-11-16 12:52:47.287' AS DateTime), 2, N'20000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (239, 74, 60, NULL, 2, CAST(N'2020-11-16 12:52:47.287' AS DateTime), 2, N'10000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (240, 75, 4, NULL, 1, CAST(N'2020-11-16 13:08:50.307' AS DateTime), 2, N'20000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (241, 75, 18, NULL, 1, CAST(N'2020-11-16 13:09:03.650' AS DateTime), 2, N'80000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (242, 75, 11, NULL, 1, CAST(N'2020-11-16 13:09:11.523' AS DateTime), 2, N'59000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (243, 75, 68, NULL, 1, CAST(N'2020-11-16 13:26:07.310' AS DateTime), 2, N'10000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (249, 78, 4, NULL, 2, CAST(N'2020-11-16 14:14:51.197' AS DateTime), 2, N'20000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (250, 78, 11, NULL, 2, CAST(N'2020-11-16 14:14:56.337' AS DateTime), 2, N'59000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (251, 79, 4, NULL, 1, CAST(N'2020-11-16 14:27:42.227' AS DateTime), 2, N'20000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (252, 79, 11, NULL, 1, CAST(N'2020-11-16 14:27:47.057' AS DateTime), 2, N'59000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (254, 78, 60, NULL, 2, CAST(N'2020-11-16 15:04:33.490' AS DateTime), 2, N'10000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (255, 76, 18, NULL, 1, CAST(N'2020-11-16 15:04:43.473' AS DateTime), 2, N'80000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (256, 76, 11, NULL, 4, CAST(N'2020-11-16 15:04:43.473' AS DateTime), 2, N'59000', 0, N'20', N'2')
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [nhanvien_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id]) VALUES (257, 76, 54, NULL, 1, CAST(N'2020-11-16 15:04:43.473' AS DateTime), 2, N'10000', 0, N'20', N'2')
SET IDENTITY_INSERT [dbo].[tbHoaDonBanHangChiTiet] OFF
SET IDENTITY_INSERT [dbo].[tbIntroduce] ON 

INSERT [dbo].[tbIntroduce] ([introduct_id], [introduce_title], [introduce_summary], [introduce_content], [introduce_image], [introduce_createdate], [introduce_update_date]) VALUES (1, N'NICENAILS', N'Các nhà mạng Việt Nam có thể tắt sóng 2G khi số lượng thuê bao sử dụng công nghệ này còn dưới 5%, mục tiêu dự kiến vào năm 2022.', N'<p class="Normal" style="margin: 0px 0px 1em; box-sizing: border-box; text-rendering: optimizespeed; line-height: 28.8px; color: #222222; font-family: arial; font-size: 18px; text-decoration-style: initial; text-decoration-color: initial;"><span style="background-color: #ffffff;">Hiện tại, theo số liệu của Cục Viễn thông, Việt Nam vẫn còn khoảng 24 triệu thuê bao 2G trên tổng số 130 triệu thuê bao đi động. Tuy nhiên, 2G là xu thế đã thoái trào và ngày càng bị thay thế bởi các công nghệ tiên tiến hơn.</span></p>', N'/uploadimages/anh_gioithieu/02112020_100532_SA_messager.png', NULL, CAST(N'2020-11-14 11:49:30.953' AS DateTime))
SET IDENTITY_INSERT [dbo].[tbIntroduce] OFF
SET IDENTITY_INSERT [dbo].[tbNewCate] ON 

INSERT [dbo].[tbNewCate] ([newcate_id], [newcate_title], [newcate_summary], [hidden], [link_seo]) VALUES (1, N'Tin hôm nay', NULL, 0, NULL)
INSERT [dbo].[tbNewCate] ([newcate_id], [newcate_title], [newcate_summary], [hidden], [link_seo]) VALUES (2, N'Tin mới', NULL, 1, NULL)
INSERT [dbo].[tbNewCate] ([newcate_id], [newcate_title], [newcate_summary], [hidden], [link_seo]) VALUES (3, N'Tin nổi bật', NULL, 0, NULL)
SET IDENTITY_INSERT [dbo].[tbNewCate] OFF
SET IDENTITY_INSERT [dbo].[tbNews] ON 

INSERT [dbo].[tbNews] ([news_id], [news_title], [news_summary], [news_image], [news_content], [newcate_id], [hidden], [active], [link_seo], [news_createdate], [news_position]) VALUES (1, N'Bản tin ngày 01/11/2020', N'Cơn mưa lớn kém giông tối 31/10 khiến trường THPT Bình Phú trên đường Trần Văn Kiểu (phường 10, quận 6) bị hư hỏng một dãy nhà. "Lúc đó gió lớn lắm, mái tôn bị kéo sụp xuống đất rất nhanh, nhiều tiếng xẹt điện phát ra", ông Nguyễn Văn Tám, bảo vệ trường nói.', N'/uploadimages/anh_tintuc/fnpnbpxq.l14.png', N'<span style="color: #222222; font-family: arial; font-size: 18px; background-color: #fcfaf6; text-decoration-style: initial; text-decoration-color: initial;">Thống kê của Reuters cho thấy Mỹ hôm 31/10 ghi nhận thêm 100.233 ca nhiễm nCoV chỉ trong 24 giờ, vượt qua kỷ lục 91.295 trường hợp được ghi nhận một ngày trước đó. Đây cũng là mức tăng cao nhất thế giới, vượt qua con số 97.894 ca trong 24 giờ được ghi nhận tại Ấn Độ hồi giữa tháng 9.</span>&nbsp;', 1, 0, 1, NULL, CAST(N'2020-11-01 15:41:09.603' AS DateTime), NULL)
INSERT [dbo].[tbNews] ([news_id], [news_title], [news_summary], [news_image], [news_content], [newcate_id], [hidden], [active], [link_seo], [news_createdate], [news_position]) VALUES (2, N'Mỹ ghi nhận kỷ lục hơn 100.000 ca nCoV một ngày', N'Mỹ báo cáo mức tăng ca nhiễm nCoV trong 24 giờ cao chưa từng có khi chỉ còn vài ngày trước bầu cử tổng thống.', N'/uploadimages/anh_tintuc/hlpm2ktf.reo.png', N'<p class="description" style="margin: 0px 0px 15px; box-sizing: border-box; text-rendering: optimizelegibility; font-size: 18px; line-height: 28.8px; color: #222222; font-family: arial; text-decoration-style: initial; text-decoration-color: initial;"><span style="background-color: #ffffff;">Mỹ báo cáo mức tăng ca nhiễm nCoV trong 24 giờ cao chưa từng có khi chỉ còn vài ngày trước bầu cử tổng thống.</span></p><p class="Normal" style="margin: 0px 0px 1em; box-sizing: border-box; text-rendering: optimizespeed; line-height: 28.8px;"><span style="background-color: #ffffff;">Thống kê của Reuters cho thấy Mỹ hôm 31/10 ghi nhận thêm 100.233 ca nhiễm nCoV chỉ trong 24 giờ, vượt qua kỷ lục 91.295 trường hợp được ghi nhận một ngày trước đó. Đây cũng là mức tăng cao nhất thế giới, vượt qua con số 97.894 ca trong 24 giờ được ghi nhận tại Ấn Độ hồi giữa tháng 9.</span></p>', 3, 0, 1, NULL, CAST(N'2020-11-01 17:30:28.147' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[tbNews] OFF
SET IDENTITY_INSERT [dbo].[tbNhapHang] ON 

INSERT [dbo].[tbNhapHang] ([nhaphang_id], [nhaphang_code], [nhaphang_createdate], [nhaphang_content], [username_id], [hidden]) VALUES (1, N'NH00001', CAST(N'2020-11-13 13:38:38.653' AS DateTime), N'nhaphang_chitiet_soluong', 1, NULL)
INSERT [dbo].[tbNhapHang] ([nhaphang_id], [nhaphang_code], [nhaphang_createdate], [nhaphang_content], [username_id], [hidden]) VALUES (2, N'NH00002', CAST(N'2020-11-13 13:50:01.137' AS DateTime), N'bdashas', 1, NULL)
INSERT [dbo].[tbNhapHang] ([nhaphang_id], [nhaphang_code], [nhaphang_createdate], [nhaphang_content], [username_id], [hidden]) VALUES (3, N'NH00003', CAST(N'2020-11-14 09:46:33.827' AS DateTime), N'nhập hàng', 1, NULL)
INSERT [dbo].[tbNhapHang] ([nhaphang_id], [nhaphang_code], [nhaphang_createdate], [nhaphang_content], [username_id], [hidden]) VALUES (4, N'NH00004', CAST(N'2020-11-14 09:54:07.130' AS DateTime), N'ưerg', 1, NULL)
INSERT [dbo].[tbNhapHang] ([nhaphang_id], [nhaphang_code], [nhaphang_createdate], [nhaphang_content], [username_id], [hidden]) VALUES (5, N'NH00005', CAST(N'2020-11-14 10:22:41.927' AS DateTime), N'nhập hàng', 1, NULL)
SET IDENTITY_INSERT [dbo].[tbNhapHang] OFF
SET IDENTITY_INSERT [dbo].[tbNhapHang_ChiTiet] ON 

INSERT [dbo].[tbNhapHang_ChiTiet] ([nhaphang_chitiet_id], [nhaphang_id], [product_id], [nhaphang_chitiet_soluong], [nhaphang_code], [nhaphang_gianhap], [nhaphang_thanhtien], [username_id]) VALUES (1, 1, 1, 2, N'NH00001', 150000, 300000, 1)
INSERT [dbo].[tbNhapHang_ChiTiet] ([nhaphang_chitiet_id], [nhaphang_id], [product_id], [nhaphang_chitiet_soluong], [nhaphang_code], [nhaphang_gianhap], [nhaphang_thanhtien], [username_id]) VALUES (4, 1, 2, 1, N'NH00001', 200000, 200000, 1)
INSERT [dbo].[tbNhapHang_ChiTiet] ([nhaphang_chitiet_id], [nhaphang_id], [product_id], [nhaphang_chitiet_soluong], [nhaphang_code], [nhaphang_gianhap], [nhaphang_thanhtien], [username_id]) VALUES (9, 3, 2, 2, N'NH00003', 100000, 200000, 1)
INSERT [dbo].[tbNhapHang_ChiTiet] ([nhaphang_chitiet_id], [nhaphang_id], [product_id], [nhaphang_chitiet_soluong], [nhaphang_code], [nhaphang_gianhap], [nhaphang_thanhtien], [username_id]) VALUES (13, 5, 1, 2, N'NH00005', 100000, 200000, 1)
SET IDENTITY_INSERT [dbo].[tbNhapHang_ChiTiet] OFF
SET IDENTITY_INSERT [dbo].[tbProduct] ON 

INSERT [dbo].[tbProduct] ([product_id], [product_position], [product_title], [product_image], [product_summary], [product_content], [product_quantum], [product_show], [product_new], [productcate_id], [title_web], [meta_title], [meta_keywords], [meta_description], [h1_seo], [link_seo], [product_chungloai], [thuonghieu_id], [meta_image], [product_representative], [product_cart], [product_price_new], [product_price], [product_promotions], [product_price_entry], [hidden]) VALUES (1, NULL, N'Sơn móng tay', N'/admin_images/up-img.png', N'đây là tóm tắt mô tả sản phẩm', N'', NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 200000, NULL, NULL, 0)
INSERT [dbo].[tbProduct] ([product_id], [product_position], [product_title], [product_image], [product_summary], [product_content], [product_quantum], [product_show], [product_new], [productcate_id], [title_web], [meta_title], [meta_keywords], [meta_description], [h1_seo], [link_seo], [product_chungloai], [thuonghieu_id], [meta_image], [product_representative], [product_cart], [product_price_new], [product_price], [product_promotions], [product_price_entry], [hidden]) VALUES (2, NULL, N'sản phẩm 2', N'/uploadimages/anh_sanpham/12112020_081847_CH_your kill.png', N'mô tả sản phẩm bvhgc', N'', NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 250000, NULL, NULL, 0)
INSERT [dbo].[tbProduct] ([product_id], [product_position], [product_title], [product_image], [product_summary], [product_content], [product_quantum], [product_show], [product_new], [productcate_id], [title_web], [meta_title], [meta_keywords], [meta_description], [h1_seo], [link_seo], [product_chungloai], [thuonghieu_id], [meta_image], [product_representative], [product_cart], [product_price_new], [product_price], [product_promotions], [product_price_entry], [hidden]) VALUES (3, NULL, N'sản phẩm 1 adba', N'/uploadimages/anh_sanpham/12112020_081652_CH_messager.png', N'dbaddbs', N'', NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[tbProduct] ([product_id], [product_position], [product_title], [product_image], [product_summary], [product_content], [product_quantum], [product_show], [product_new], [productcate_id], [title_web], [meta_title], [meta_keywords], [meta_description], [h1_seo], [link_seo], [product_chungloai], [thuonghieu_id], [meta_image], [product_representative], [product_cart], [product_price_new], [product_price], [product_promotions], [product_price_entry], [hidden]) VALUES (4, NULL, N'Sơn đỏ ', N'/admin_images/up-img.png', N'Mô tả sản phẩm', N'', NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 200000, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[tbProduct] OFF
SET IDENTITY_INSERT [dbo].[tbProductCate] ON 

INSERT [dbo].[tbProductCate] ([productcate_id], [productcate_position], [productcate_title], [productcate_show], [productgroup_id], [title_web], [meta_title], [meta_keywords], [meta_description], [h1_seo], [link_seo], [productcate_parent], [productcate_content], [meta_image], [active], [hidden]) VALUES (3, NULL, N'Sơn móng tay', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[tbProductCate] ([productcate_id], [productcate_position], [productcate_title], [productcate_show], [productgroup_id], [title_web], [meta_title], [meta_keywords], [meta_description], [h1_seo], [link_seo], [productcate_parent], [productcate_content], [meta_image], [active], [hidden]) VALUES (4, NULL, N'Sơn dưỡng', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[tbProductCate] OFF
SET IDENTITY_INSERT [dbo].[tbSlide] ON 

INSERT [dbo].[tbSlide] ([slide_id], [slide_image], [slide_title], [slide_title1], [slide_link], [slide_summary], [slide_content], [hidden]) VALUES (1, N'/uploadimages/anh_slide/14112020_115231_SA_show team.png', NULL, NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[tbSlide] ([slide_id], [slide_image], [slide_title], [slide_title1], [slide_link], [slide_summary], [slide_content], [hidden]) VALUES (2, N'/uploadimages/anh_slide/u5wblhh2.pas.png', NULL, NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[tbSlide] OFF
SET IDENTITY_INSERT [dbo].[tbXuatHang] ON 

INSERT [dbo].[tbXuatHang] ([xuathang_id], [xuathang_code], [xuathang_createdate], [xuathang_content], [username_id], [hidden]) VALUES (2, N'XH00001', CAST(N'2020-11-13 20:00:25.690' AS DateTime), N'Xuất hàng dịch vụ', 1, NULL)
INSERT [dbo].[tbXuatHang] ([xuathang_id], [xuathang_code], [xuathang_createdate], [xuathang_content], [username_id], [hidden]) VALUES (3, N'XH00002', CAST(N'2020-11-14 10:11:58.620' AS DateTime), N'xuất hàng dịch vụ', 1, NULL)
SET IDENTITY_INSERT [dbo].[tbXuatHang] OFF
SET IDENTITY_INSERT [dbo].[tbXuatHang_ChiTiet] ON 

INSERT [dbo].[tbXuatHang_ChiTiet] ([xuathang_chitiet_id], [xuathang_id], [product_id], [xuathang_chitiet_soluong], [username_id]) VALUES (3, 2, 1, 5, 1)
INSERT [dbo].[tbXuatHang_ChiTiet] ([xuathang_chitiet_id], [xuathang_id], [product_id], [xuathang_chitiet_soluong], [username_id]) VALUES (4, 2, 2, 5, 1)
INSERT [dbo].[tbXuatHang_ChiTiet] ([xuathang_chitiet_id], [xuathang_id], [product_id], [xuathang_chitiet_soluong], [username_id]) VALUES (7, 3, 1, 1, 1)
INSERT [dbo].[tbXuatHang_ChiTiet] ([xuathang_chitiet_id], [xuathang_id], [product_id], [xuathang_chitiet_soluong], [username_id]) VALUES (8, 3, 2, 2, 1)
SET IDENTITY_INSERT [dbo].[tbXuatHang_ChiTiet] OFF
SET IDENTITY_INSERT [hel13271_nails].[tbNhomDichVu] ON 

INSERT [hel13271_nails].[tbNhomDichVu] ([dvcate_id], [dvcate_name], [position]) VALUES (1, N'Nails cơ bản', 2)
INSERT [hel13271_nails].[tbNhomDichVu] ([dvcate_id], [dvcate_name], [position]) VALUES (2, N'Trang trí móng', 6)
INSERT [hel13271_nails].[tbNhomDichVu] ([dvcate_id], [dvcate_name], [position]) VALUES (3, N'Gội đầu', 1)
INSERT [hel13271_nails].[tbNhomDichVu] ([dvcate_id], [dvcate_name], [position]) VALUES (4, N'Phụ kiện', 5)
INSERT [hel13271_nails].[tbNhomDichVu] ([dvcate_id], [dvcate_name], [position]) VALUES (5, N'Sơn', 3)
INSERT [hel13271_nails].[tbNhomDichVu] ([dvcate_id], [dvcate_name], [position]) VALUES (6, N'Nối móng', 4)
SET IDENTITY_INSERT [hel13271_nails].[tbNhomDichVu] OFF
ALTER TABLE [dbo].[admin_AccessGroupUserForm]  WITH CHECK ADD FOREIGN KEY([form_id])
REFERENCES [dbo].[admin_Form] ([form_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[admin_AccessGroupUserForm]  WITH CHECK ADD FOREIGN KEY([groupuser_id])
REFERENCES [dbo].[admin_GroupUser] ([groupuser_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[admin_AccessGroupUserModule]  WITH CHECK ADD FOREIGN KEY([groupuser_id])
REFERENCES [dbo].[admin_GroupUser] ([groupuser_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[admin_AccessGroupUserModule]  WITH CHECK ADD FOREIGN KEY([module_id])
REFERENCES [dbo].[admin_Module] ([module_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[admin_AccessUserForm]  WITH CHECK ADD FOREIGN KEY([form_id])
REFERENCES [dbo].[admin_Form] ([form_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[admin_AccessUserForm]  WITH CHECK ADD FOREIGN KEY([username_id])
REFERENCES [dbo].[admin_User] ([username_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[admin_Form]  WITH CHECK ADD FOREIGN KEY([module_id])
REFERENCES [dbo].[admin_Module] ([module_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[admin_User]  WITH CHECK ADD FOREIGN KEY([groupuser_id])
REFERENCES [dbo].[admin_GroupUser] ([groupuser_id])
ON UPDATE CASCADE
GO
