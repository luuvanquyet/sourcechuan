USE [hel13271_Nails]
GO
/****** Object:  User [hel13271_nails]    Script Date: 27/11/2020 9:05:24 PM ******/
CREATE USER [hel13271_nails] FOR LOGIN [hel13271_nails] WITH DEFAULT_SCHEMA=[hel13271_nails]
GO
ALTER ROLE [db_ddladmin] ADD MEMBER [hel13271_nails]
GO
ALTER ROLE [db_backupoperator] ADD MEMBER [hel13271_nails]
GO
ALTER ROLE [db_datareader] ADD MEMBER [hel13271_nails]
GO
ALTER ROLE [db_datawriter] ADD MEMBER [hel13271_nails]
GO
/****** Object:  Schema [hel13271_nails]    Script Date: 27/11/2020 9:05:24 PM ******/
CREATE SCHEMA [hel13271_nails]
GO
/****** Object:  Table [dbo].[admin_AccessGroupUserForm]    Script Date: 27/11/2020 9:05:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[admin_AccessGroupUserForm](
	[guf_id] [int] IDENTITY(1,1) NOT NULL,
	[guf_active] [bit] NULL,
	[groupuser_id] [int] NULL,
	[form_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[guf_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[admin_AccessGroupUserModule]    Script Date: 27/11/2020 9:05:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[admin_AccessGroupUserModule](
	[gum_id] [int] IDENTITY(1,1) NOT NULL,
	[gum_active] [bit] NULL,
	[groupuser_id] [int] NULL,
	[module_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[gum_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[admin_AccessUserForm]    Script Date: 27/11/2020 9:05:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[admin_AccessUserForm](
	[uf_id] [int] IDENTITY(1,1) NOT NULL,
	[uf_active] [bit] NULL,
	[username_id] [int] NULL,
	[form_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[uf_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[admin_Form]    Script Date: 27/11/2020 9:05:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[admin_Form](
	[form_id] [int] IDENTITY(1,1) NOT NULL,
	[form_position] [int] NULL,
	[form_name] [nvarchar](max) NULL,
	[form_link] [nvarchar](max) NULL,
	[form_active] [bit] NULL,
	[module_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[form_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[admin_GroupUser]    Script Date: 27/11/2020 9:05:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[admin_GroupUser](
	[groupuser_id] [int] IDENTITY(1,1) NOT NULL,
	[groupuser_name] [nvarchar](max) NULL,
	[groupuser_active] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[groupuser_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[admin_Module]    Script Date: 27/11/2020 9:05:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[admin_Module](
	[module_id] [int] IDENTITY(1,1) NOT NULL,
	[module_position] [int] NULL,
	[module_name] [nvarchar](max) NULL,
	[module_icon] [nvarchar](max) NULL,
	[module_active] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[module_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[admin_User]    Script Date: 27/11/2020 9:05:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[admin_User](
	[username_id] [int] IDENTITY(1,1) NOT NULL,
	[username_username] [nvarchar](max) NULL,
	[username_password] [nvarchar](max) NULL,
	[username_fullname] [nvarchar](max) NULL,
	[username_gender] [bit] NULL,
	[username_phone] [nvarchar](max) NULL,
	[username_email] [nvarchar](max) NULL,
	[username_active] [bit] NULL,
	[groupuser_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[username_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbChuongTrinhKhuyenMai]    Script Date: 27/11/2020 9:05:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbChuongTrinhKhuyenMai](
	[khuyenmai_id] [int] IDENTITY(1,1) NOT NULL,
	[khuyenmai_name] [nvarchar](max) NULL,
	[khuyenmai_content] [nvarchar](max) NULL,
	[khuyenmai_percent] [nvarchar](max) NULL,
	[khuyenmai_image] [nvarchar](max) NULL,
	[khuyenmai_tungay] [datetime] NULL,
	[khuyenmai_denngay] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[khuyenmai_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbCity]    Script Date: 27/11/2020 9:05:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbCity](
	[city_id] [int] IDENTITY(1,1) NOT NULL,
	[city_name] [nvarchar](max) NULL,
	[city_position] [int] NULL,
	[city_show] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[city_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbCustomerAccount]    Script Date: 27/11/2020 9:05:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbCustomerAccount](
	[customer_id] [int] IDENTITY(1,1) NOT NULL,
	[customer_firstname] [nvarchar](max) NULL,
	[customer_lastname] [nvarchar](max) NULL,
	[customer_fullname] [nvarchar](max) NULL,
	[customer_phone] [nvarchar](max) NULL,
	[customer_email] [nvarchar](max) NULL,
	[customer_address] [nvarchar](max) NULL,
	[hidden] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[customer_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbDichVu]    Script Date: 27/11/2020 9:05:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbDichVu](
	[dichvu_id] [int] IDENTITY(1,1) NOT NULL,
	[dichvu_title] [nvarchar](max) NULL,
	[dichvu_content] [nvarchar](max) NULL,
	[dichvu_price] [nvarchar](max) NULL,
	[dichvu_image] [nvarchar](max) NULL,
	[dvcate_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[dichvu_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbHoaDonBanHang]    Script Date: 27/11/2020 9:05:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbHoaDonBanHang](
	[hoadon_id] [int] IDENTITY(1,1) NOT NULL,
	[hoadon_code] [nvarchar](max) NULL,
	[khachhang_name] [nvarchar](max) NULL,
	[hoadon_tongtien] [nvarchar](max) NULL,
	[hoadon_giamgia] [nvarchar](max) NULL,
	[hoadon_phaitra] [nvarchar](max) NULL,
	[hoadon_createdate] [datetime] NULL,
	[username_id] [int] NULL,
	[hoadon_tongtiengiam] [nvarchar](max) NULL,
	[active] [bit] NULL,
	[khachhang_id] [int] NULL,
	[khuyenmai_id] [nvarchar](max) NULL,
	[hoadon_giothanhtoan] [datetime] NULL,
	[hidden] [bit] NULL,
	[nhanvien_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[hoadon_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbHoaDonBanHangChiTiet]    Script Date: 27/11/2020 9:05:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbHoaDonBanHangChiTiet](
	[hdct_id] [int] IDENTITY(1,1) NOT NULL,
	[hoadon_id] [int] NULL,
	[dichvu_id] [int] NULL,
	[product_id] [int] NULL,
	[hdct_soluong] [int] NULL,
	[hdct_createdate] [datetime] NULL,
	[username_id] [int] NULL,
	[hdct_price] [nvarchar](max) NULL,
	[hidden] [bit] NULL,
	[hdct_giamgia] [nvarchar](max) NULL,
	[khuyenmai_id] [nvarchar](max) NULL,
	[nhanvien_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[hdct_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbIntroduce]    Script Date: 27/11/2020 9:05:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbIntroduce](
	[introduct_id] [int] IDENTITY(1,1) NOT NULL,
	[introduce_title] [nvarchar](max) NULL,
	[introduce_summary] [nvarchar](max) NULL,
	[introduce_content] [nvarchar](max) NULL,
	[introduce_image] [nvarchar](max) NULL,
	[introduce_createdate] [datetime] NULL,
	[introduce_update_date] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[introduct_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbNewCate]    Script Date: 27/11/2020 9:05:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbNewCate](
	[newcate_id] [int] IDENTITY(1,1) NOT NULL,
	[newcate_title] [nvarchar](max) NULL,
	[newcate_summary] [nvarchar](max) NULL,
	[hidden] [bit] NULL,
	[link_seo] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[newcate_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbNews]    Script Date: 27/11/2020 9:05:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbNews](
	[news_id] [int] IDENTITY(1,1) NOT NULL,
	[news_title] [nvarchar](max) NULL,
	[news_summary] [nvarchar](max) NULL,
	[news_image] [nvarchar](max) NULL,
	[news_content] [nvarchar](max) NULL,
	[newcate_id] [int] NULL,
	[hidden] [bit] NULL,
	[active] [bit] NULL,
	[link_seo] [nvarchar](max) NULL,
	[news_createdate] [datetime] NULL,
	[news_position] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[news_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbNhapHang]    Script Date: 27/11/2020 9:05:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbNhapHang](
	[nhaphang_id] [int] IDENTITY(1,1) NOT NULL,
	[nhaphang_code] [nvarchar](max) NULL,
	[nhaphang_createdate] [datetime] NULL,
	[nhaphang_content] [nvarchar](max) NULL,
	[username_id] [int] NULL,
	[hidden] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[nhaphang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbNhapHang_ChiTiet]    Script Date: 27/11/2020 9:05:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbNhapHang_ChiTiet](
	[nhaphang_chitiet_id] [int] IDENTITY(1,1) NOT NULL,
	[nhaphang_id] [int] NULL,
	[product_id] [int] NULL,
	[nhaphang_chitiet_soluong] [int] NULL,
	[nhaphang_code] [nvarchar](max) NULL,
	[nhaphang_gianhap] [int] NULL,
	[nhaphang_thanhtien] [int] NULL,
	[username_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[nhaphang_chitiet_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbProduct]    Script Date: 27/11/2020 9:05:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbProduct](
	[product_id] [int] IDENTITY(1,1) NOT NULL,
	[product_position] [int] NULL,
	[product_title] [nvarchar](max) NULL,
	[product_image] [nvarchar](max) NULL,
	[product_summary] [nvarchar](max) NULL,
	[product_content] [nvarchar](max) NULL,
	[product_quantum] [int] NULL,
	[product_show] [bit] NULL,
	[product_new] [bit] NULL,
	[productcate_id] [int] NULL,
	[title_web] [nvarchar](max) NULL,
	[meta_title] [nvarchar](max) NULL,
	[meta_keywords] [nvarchar](max) NULL,
	[meta_description] [nvarchar](max) NULL,
	[h1_seo] [nvarchar](max) NULL,
	[link_seo] [nvarchar](max) NULL,
	[product_chungloai] [nvarchar](max) NULL,
	[thuonghieu_id] [int] NULL,
	[meta_image] [nvarchar](max) NULL,
	[product_representative] [int] NULL,
	[product_cart] [int] NULL,
	[product_price_new] [int] NULL,
	[product_price] [int] NULL,
	[product_promotions] [int] NULL,
	[product_price_entry] [int] NULL,
	[hidden] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[product_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbProductCate]    Script Date: 27/11/2020 9:05:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbProductCate](
	[productcate_id] [int] IDENTITY(1,1) NOT NULL,
	[productcate_position] [int] NULL,
	[productcate_title] [nvarchar](max) NULL,
	[productcate_show] [bit] NULL,
	[productgroup_id] [int] NULL,
	[title_web] [nvarchar](max) NULL,
	[meta_title] [nvarchar](max) NULL,
	[meta_keywords] [nvarchar](max) NULL,
	[meta_description] [nvarchar](max) NULL,
	[h1_seo] [nvarchar](max) NULL,
	[link_seo] [nvarchar](max) NULL,
	[productcate_parent] [int] NULL,
	[productcate_content] [nvarchar](max) NULL,
	[meta_image] [nvarchar](max) NULL,
	[active] [bit] NULL,
	[hidden] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[productcate_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbSlide]    Script Date: 27/11/2020 9:05:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbSlide](
	[slide_id] [int] IDENTITY(1,1) NOT NULL,
	[slide_image] [nvarchar](max) NULL,
	[slide_title] [nvarchar](max) NULL,
	[slide_title1] [nvarchar](max) NULL,
	[slide_link] [nvarchar](max) NULL,
	[slide_summary] [nvarchar](max) NULL,
	[slide_content] [nvarchar](max) NULL,
	[hidden] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[slide_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbXuatHang]    Script Date: 27/11/2020 9:05:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbXuatHang](
	[xuathang_id] [int] IDENTITY(1,1) NOT NULL,
	[xuathang_code] [nvarchar](max) NULL,
	[xuathang_createdate] [datetime] NULL,
	[xuathang_content] [nvarchar](max) NULL,
	[username_id] [int] NULL,
	[hidden] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[xuathang_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbXuatHang_ChiTiet]    Script Date: 27/11/2020 9:05:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbXuatHang_ChiTiet](
	[xuathang_chitiet_id] [int] IDENTITY(1,1) NOT NULL,
	[xuathang_id] [int] NULL,
	[product_id] [int] NULL,
	[xuathang_chitiet_soluong] [int] NULL,
	[username_id] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[xuathang_chitiet_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [hel13271_nails].[tbChiPhiHoatDong]    Script Date: 27/11/2020 9:05:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hel13271_nails].[tbChiPhiHoatDong](
	[chiphi_id] [int] IDENTITY(1,1) NOT NULL,
	[nhomhd_id] [int] NULL,
	[chiphi_content] [nvarchar](max) NULL,
	[chiphi_sotien] [int] NULL,
	[chiphi_createdate] [datetime] NULL,
	[chiphi_month] [nvarchar](max) NULL,
	[chiphi_update_date] [datetime] NULL,
	[hidden] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[chiphi_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [hel13271_nails].[tbNhomChiPhiHoatDong]    Script Date: 27/11/2020 9:05:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hel13271_nails].[tbNhomChiPhiHoatDong](
	[nhomhd_id] [int] IDENTITY(1,1) NOT NULL,
	[nhomhd_name] [nvarchar](max) NULL,
	[hidden] [bit] NULL,
PRIMARY KEY CLUSTERED 
(
	[nhomhd_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [hel13271_nails].[tbNhomDichVu]    Script Date: 27/11/2020 9:05:25 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [hel13271_nails].[tbNhomDichVu](
	[dvcate_id] [int] IDENTITY(1,1) NOT NULL,
	[dvcate_name] [nvarchar](max) NULL,
	[position] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[dvcate_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[admin_AccessGroupUserForm] ON 

INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (1, 1, 1, 1)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (2, 1, 1, 2)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (3, 1, 1, 3)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (4, 1, 1, 4)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (5, 1, 1, 5)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (6, 1, 1, 6)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (7, 1, 1, 7)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (8, 1, 1, 8)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (9, 1, 1, 9)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (10, 1, 1, 10)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (11, 1, 1, 11)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (12, 1, 1, 12)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (13, 1, 1, 13)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (14, 1, 1, 14)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (15, 1, 1, 15)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (16, 1, 1, 16)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (17, 1, 2, 4)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (18, 1, 2, 5)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (19, 1, 2, 6)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (20, 1, 2, 8)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (21, 1, 2, 9)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (22, 1, 2, 10)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (23, 1, 2, 11)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (24, 1, 2, 12)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (25, 1, 2, 15)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (26, 1, 2, 16)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (27, 1, 2, 13)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (28, 1, 2, 14)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (29, 1, 2, 1)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (30, 1, 3, 14)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (31, 1, 1, 17)
INSERT [dbo].[admin_AccessGroupUserForm] ([guf_id], [guf_active], [groupuser_id], [form_id]) VALUES (32, 1, 2, 17)
SET IDENTITY_INSERT [dbo].[admin_AccessGroupUserForm] OFF
SET IDENTITY_INSERT [dbo].[admin_AccessGroupUserModule] ON 

INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (1, 1, 1, 1)
INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (2, 1, 1, 2)
INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (3, 1, 1, 3)
INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (4, 1, 1, 4)
INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (5, 1, 1, 5)
INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (6, 1, 1, 6)
INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (7, 1, 1, 7)
INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (8, 1, 2, 2)
INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (9, 1, 2, 3)
INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (10, 1, 2, 4)
INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (11, 0, 2, 5)
INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (12, 1, 2, 6)
INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (13, 1, 2, 7)
INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (14, 1, 2, 1)
INSERT [dbo].[admin_AccessGroupUserModule] ([gum_id], [gum_active], [groupuser_id], [module_id]) VALUES (15, 1, 3, 7)
SET IDENTITY_INSERT [dbo].[admin_AccessGroupUserModule] OFF
SET IDENTITY_INSERT [dbo].[admin_AccessUserForm] ON 

INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (1, 1, 1, 1)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (2, 1, 1, 2)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (3, 1, 1, 3)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (4, 1, 1, 4)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (5, 1, 1, 5)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (6, 1, 1, 6)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (7, 1, 1, 7)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (8, 1, 1, 8)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (9, 1, 1, 9)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (10, 1, 1, 10)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (11, 1, 1, 11)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (12, 1, 1, 12)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (13, 1, 1, 13)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (14, 1, 1, 14)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (15, 1, 1, 15)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (16, 1, 1, 16)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (17, 1, 2, 4)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (18, 1, 2, 5)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (19, 1, 2, 6)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (20, 1, 2, 8)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (21, 1, 2, 9)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (22, 1, 2, 10)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (23, 1, 2, 11)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (24, 1, 2, 12)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (25, 1, 2, 15)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (26, 1, 2, 16)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (27, 1, 2, 13)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (28, 1, 2, 14)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (29, 1, 2, 1)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (30, 1, 5, 14)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (31, 1, 1, 17)
INSERT [dbo].[admin_AccessUserForm] ([uf_id], [uf_active], [username_id], [form_id]) VALUES (32, 1, 2, 17)
SET IDENTITY_INSERT [dbo].[admin_AccessUserForm] OFF
SET IDENTITY_INSERT [dbo].[admin_Form] ON 

INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (1, 1, N'Quản lý phân quyền', N'admin-access', 1, 1)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (2, 2, N'Quản lý Module', N'admin-module', 1, 1)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (3, 3, N'Quản lý Form', N'admin-form', 1, 1)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (4, 1, N'Quản lý tài khoản', N'admin-account', 1, 2)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (5, 1, N'Quản lý nhóm sản phẩm', N'admin-quan-ly-nhom-san-pham', 1, 3)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (6, 2, N'Quản lý sản phẩm', N'admin-quan-ly-san-pham', 1, 3)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (7, 3, N'Quản lý nhóm tin tức', N'admin-new-cate', 0, 4)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (8, 4, N'Quản lý tin tức', N'admin-news', 1, 4)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (9, 2, N'Quản lý slide', N'admin-slide', 1, 4)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (10, 1, N'Quản lý giới thiệu', N'admin-introduce', 1, 4)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (11, 5, N'Chương trình khuyến mãi', N'admin-chuong-trinh-khuyen-mai', 1, 4)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (12, 6, N'Tài khoản khách hàng', N'admin-tai-khoan-khach-hang', 1, 4)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (13, 1, N'Quản lý dịch vụ', N'admin-quan-ly-dich-vu', 1, 6)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (14, 1, N'Hóa đơn bán hàng', N'admin-hoa-don-ban-hang', 1, 7)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (15, 1, N'Quản lý nhập hàng', N'admin-quan-ly-nhap-hang', 1, 5)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (16, 2, N'Quản lý xuất hàng', N'admin-quan-ly-xuat-hang', 1, 5)
INSERT [dbo].[admin_Form] ([form_id], [form_position], [form_name], [form_link], [form_active], [module_id]) VALUES (17, 1, N'Quản lý bộ phận', N'admin-groupuser', 1, 2)
SET IDENTITY_INSERT [dbo].[admin_Form] OFF
SET IDENTITY_INSERT [dbo].[admin_GroupUser] ON 

INSERT [dbo].[admin_GroupUser] ([groupuser_id], [groupuser_name], [groupuser_active]) VALUES (1, N'root', 1)
INSERT [dbo].[admin_GroupUser] ([groupuser_id], [groupuser_name], [groupuser_active]) VALUES (2, N'Admin', 1)
INSERT [dbo].[admin_GroupUser] ([groupuser_id], [groupuser_name], [groupuser_active]) VALUES (3, N'Nhân viên', 1)
INSERT [dbo].[admin_GroupUser] ([groupuser_id], [groupuser_name], [groupuser_active]) VALUES (4, N'Thợ nail', 1)
SET IDENTITY_INSERT [dbo].[admin_GroupUser] OFF
SET IDENTITY_INSERT [dbo].[admin_Module] ON 

INSERT [dbo].[admin_Module] ([module_id], [module_position], [module_name], [module_icon], [module_active]) VALUES (1, 1, N'Phân quyền', N'', 1)
INSERT [dbo].[admin_Module] ([module_id], [module_position], [module_name], [module_icon], [module_active]) VALUES (2, 2, N'Tài khoản', N'fas fa-users', 1)
INSERT [dbo].[admin_Module] ([module_id], [module_position], [module_name], [module_icon], [module_active]) VALUES (3, 4, N'Quản lý sản phẩm', N'', 1)
INSERT [dbo].[admin_Module] ([module_id], [module_position], [module_name], [module_icon], [module_active]) VALUES (4, 3, N'Quản lý website', N'', 1)
INSERT [dbo].[admin_Module] ([module_id], [module_position], [module_name], [module_icon], [module_active]) VALUES (5, 5, N'Quản lý Nhập-Xuất hàng', N'', 1)
INSERT [dbo].[admin_Module] ([module_id], [module_position], [module_name], [module_icon], [module_active]) VALUES (6, 6, N'Quản lý dịch vụ', N'', 1)
INSERT [dbo].[admin_Module] ([module_id], [module_position], [module_name], [module_icon], [module_active]) VALUES (7, 7, N'Hóa đơn', NULL, 1)
SET IDENTITY_INSERT [dbo].[admin_Module] OFF
SET IDENTITY_INSERT [dbo].[admin_User] ON 

INSERT [dbo].[admin_User] ([username_id], [username_username], [username_password], [username_fullname], [username_gender], [username_phone], [username_email], [username_active], [groupuser_id]) VALUES (1, N'root', N'12378248145104161527610811213823414203124130', N'ROOT', 1, N'01634057167', N'qutienpro@gmail.com', 1, 1)
INSERT [dbo].[admin_User] ([username_id], [username_username], [username_password], [username_fullname], [username_gender], [username_phone], [username_email], [username_active], [groupuser_id]) VALUES (2, N'admin', N'12378248145104161527610811213823414203124130', N'Quản trị', 1, N'12345', N'quantri@hifiveplus.vn', 1, 2)
INSERT [dbo].[admin_User] ([username_id], [username_username], [username_password], [username_fullname], [username_gender], [username_phone], [username_email], [username_active], [groupuser_id]) VALUES (4, N'quyetlv', N'12378248145104161527610811213823414203124130', N'Lưu Văn Quyết', NULL, N'0334798366', N'quyet@gmail.com', 1, 3)
INSERT [dbo].[admin_User] ([username_id], [username_username], [username_password], [username_fullname], [username_gender], [username_phone], [username_email], [username_active], [groupuser_id]) VALUES (5, N'hoanglan', N'12378248145104161527610811213823414203124130', N'Hoàng Lan', NULL, N'0919698094', N'hoanglan6192@gmail.com', 1, 3)
INSERT [dbo].[admin_User] ([username_id], [username_username], [username_password], [username_fullname], [username_gender], [username_phone], [username_email], [username_active], [groupuser_id]) VALUES (6, N'kimtu', N'12378248145104161527610811213823414203124130', N'Đặng Thị Kim Tú', NULL, N'0905912552', N'kimtu92@gmail.com', 1, 3)
INSERT [dbo].[admin_User] ([username_id], [username_username], [username_password], [username_fullname], [username_gender], [username_phone], [username_email], [username_active], [groupuser_id]) VALUES (7, N'nhai', N'', N'Nhài', NULL, N'0392347199', N'nhai@gmail.com', 1, 4)
INSERT [dbo].[admin_User] ([username_id], [username_username], [username_password], [username_fullname], [username_gender], [username_phone], [username_email], [username_active], [groupuser_id]) VALUES (8, N'', N'', N'Đoàn T Ánh Tuyết', NULL, N'0329419209', N'', 1, 4)
SET IDENTITY_INSERT [dbo].[admin_User] OFF
SET IDENTITY_INSERT [dbo].[tbChuongTrinhKhuyenMai] ON 

INSERT [dbo].[tbChuongTrinhKhuyenMai] ([khuyenmai_id], [khuyenmai_name], [khuyenmai_content], [khuyenmai_percent], [khuyenmai_image], [khuyenmai_tungay], [khuyenmai_denngay]) VALUES (2, N'Khuyến mãi khai trương', N'Nội dung khuyến mãi', N'20', N'/uploadimages/anh_khuyenmai/14112020_013116_CH_z2159486269357_fed878df4bb9a2d8c59f0dd58dadfb3c.jpg', CAST(N'2020-11-16 00:00:00.000' AS DateTime), CAST(N'2020-11-22 00:00:00.000' AS DateTime))
INSERT [dbo].[tbChuongTrinhKhuyenMai] ([khuyenmai_id], [khuyenmai_name], [khuyenmai_content], [khuyenmai_percent], [khuyenmai_image], [khuyenmai_tungay], [khuyenmai_denngay]) VALUES (3, N'Khuyến mãi', N'', N'5', N'/admin_images/up-img.png', CAST(N'2020-11-10 00:00:00.000' AS DateTime), CAST(N'2020-11-13 00:00:00.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[tbChuongTrinhKhuyenMai] OFF
SET IDENTITY_INSERT [dbo].[tbCity] ON 

INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (1, N'An Giang', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (2, N'Bà Rịa - Vũng Tàu
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (3, N'Bắc Giang
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (4, N'Bắc Kạn
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (5, N'Bạc Liêu
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (6, N'Bắc Ninh
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (7, N'Bến Tre
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (8, N'Bình Định
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (9, N'Bình Dương
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (10, N'Bình Phước
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (11, N'Bình Thuận
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (12, N'Cà Mau
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (13, N'Cao Bằng
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (14, N'Đắk Lắk
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (15, N'Đắk Nông
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (16, N'Điện Biên
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (17, N'Đồng Nai
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (18, N'Đồng Tháp
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (19, N'Gia Lai
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (20, N'Hà Giang
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (21, N'Hà Nam
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (22, N'Hà Tĩnh
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (23, N'Hải Dương
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (24, N'Hậu Giang
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (25, N'Hòa Bình
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (26, N'Hưng Yên
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (27, N'Khánh Hòa
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (28, N'Kiên Giang
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (29, N'Kon Tum
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (30, N'Lai Châu
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (31, N'Lâm Đồng
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (32, N'Lạng Sơn
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (33, N'Lào Cai
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (34, N'Long An
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (35, N'Nam Định
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (36, N'Nghệ An
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (37, N'Ninh Bình
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (38, N'Ninh Thuận
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (39, N'Phú Thọ
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (40, N'Quảng Bình
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (41, N'Quảng Nam
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (42, N'Quảng Ngãi
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (43, N'Quảng Ninh
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (44, N'Quảng Trị
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (45, N'Sóc Trăng
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (46, N'Sơn La
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (47, N'Tây Ninh
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (48, N'Thái Bình
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (49, N'Thái Nguyên
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (50, N'Thanh Hóa
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (51, N'Thừa Thiên Huế
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (52, N'Tiền Giang
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (53, N'Trà Vinh
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (54, N'Tuyên Quang
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (55, N'Vĩnh Long
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (56, N'Vĩnh Phúc
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (57, N'Yên Bái
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (58, N'Phú Yên
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (59, N'Cần Thơ
', NULL, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (60, N'Đà Nẵng
', 1, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (61, N'Hải Phòng
', 2, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (62, N'Hà Nội
', 3, NULL)
INSERT [dbo].[tbCity] ([city_id], [city_name], [city_position], [city_show]) VALUES (63, N'TP HCM
', 4, NULL)
SET IDENTITY_INSERT [dbo].[tbCity] OFF
SET IDENTITY_INSERT [dbo].[tbCustomerAccount] ON 

INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (1, NULL, NULL, N'Khách hàng 1', N'1', NULL, N'Đà Nẵng', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (2, NULL, NULL, N'Khách hàng 2', N'2', NULL, N'Đà Nẵng', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (3, NULL, NULL, N'Khách hàng 3', N'3', NULL, N'Đà Nẵng', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (4, NULL, NULL, N'Khách hàng 4', N'4', NULL, N'Đà nẵng', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (5, NULL, NULL, N'Khách hàng 5', N'5', NULL, N'Đà Nẵng', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (6, NULL, NULL, N'Khách hàng 6', N'6', NULL, N'Đà Nẵng', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (7, NULL, NULL, N'Khách hàng 7', N'7', NULL, N'Đà Nẵng', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (8, NULL, NULL, N'Khách hàng 8', N'8', NULL, N'Đà Nẵng', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (9, NULL, NULL, N'Khách hàng 9', N'9', NULL, N'Đà Nẵng', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (10, NULL, NULL, N'Khách hàng 10', N'10', NULL, N'Đà Nẵng', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (11, NULL, NULL, N'Kim thoa', N'123', NULL, N'', 1)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (12, NULL, NULL, N'Kim chỉ', N'234', NULL, N'', 1)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (13, NULL, NULL, N'Kim Tú', N'0905912552', NULL, N'', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (14, NULL, NULL, N'kim châm', N'12', NULL, N'', 1)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (15, NULL, NULL, N'Kim trọng', N'321', NULL, N'', 1)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (16, NULL, NULL, N'Khánh Chi', N'0905339033', NULL, N'', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (17, NULL, NULL, N'Duyên', N'0966176134', NULL, N'', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (18, NULL, NULL, N'Kiều Thuận', N'0905439392', NULL, N'', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (19, NULL, NULL, N'Tiên Tiên', N'0935802206', NULL, N'', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (20, NULL, NULL, N'Huệ', N'0905973667', NULL, N'', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (21, NULL, NULL, N'Ái Quỳnh', N'0935323384', NULL, N'', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (22, NULL, NULL, N'Minh Anh', N'0984224164', NULL, N'', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (23, NULL, NULL, N'Trần Thị Vĩnh', N'0934856313', NULL, N'', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (24, NULL, NULL, N'Trần Thị Vân', N'0794484246', NULL, N'', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (25, NULL, NULL, N'Thoa', N'0941284431', NULL, N'', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (26, NULL, NULL, N'Bích Vân', N'0788381974', NULL, N'', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (27, NULL, NULL, N'Quốc Bảo', N'0123456789', NULL, N'', 0)
INSERT [dbo].[tbCustomerAccount] ([customer_id], [customer_firstname], [customer_lastname], [customer_fullname], [customer_phone], [customer_email], [customer_address], [hidden]) VALUES (28, NULL, NULL, N'Duyên', N'0918334674', NULL, N'', 0)
SET IDENTITY_INSERT [dbo].[tbCustomerAccount] OFF
SET IDENTITY_INSERT [dbo].[tbDichVu] ON 

INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (1, N'Tẩy da chết chân 30k', N'', N'30000', N'/admin_images/up-img.png', 1)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (2, N'Chà gót chân 50k', N'', N'50000', N'/admin_images/up-img.png', 1)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (3, N'Phá Gel 30k', N'', N'30000', N'/uploadimages/anh_dichvu/14112020_104850_SA_your icon.png', 1)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (4, N'Sửa móng 20k', N'Lấy khóe, nhặt da, chỉnh form móng, dũa máy', N'20000', N'/uploadimages/anh_dichvu/14112020_104715_SA_messager.png', 1)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (5, N'Ngâm chân thảo dược Massega 80k', N'', N'80000', N'/admin_images/up-img.png', 1)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (6, N'Gội đầu thường 25k', N'', N'25000', N'/admin_images/up-img.png', 3)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (7, N'Gội đầu cập 50k', N'', N'50000', N'/admin_images/up-img.png', 3)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (8, N'Rửa mặt tẩy da chết 40k', N'', N'40000', N'/admin_images/up-img.png', 3)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (9, N'Sơn bóng móng 25k', N'', N'25000', N'/admin_images/up-img.png', 5)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (10, N'Sơn thường 30k', N'', N'30000', N'/admin_images/up-img.png', 5)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (11, N'Sơn Gel 59k', N'', N'59000', N'/admin_images/up-img.png', 5)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (12, N'Sơn cứng móng 15k', N'', N'15000', N'/admin_images/up-img.png', 5)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (13, N'Sơn nhũ 60k', N'', N'60000', N'/admin_images/up-img.png', 5)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (14, N'Sơn mắt mèo 70k', N'', N'70000', N'/admin_images/up-img.png', 5)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (15, N'Dán móng giả 60k', N'', N'60000', N'/admin_images/up-img.png', 6)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (16, N'Đắp Gel 150k', N'', N'150000', N'/admin_images/up-img.png', 6)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (17, N'Đắp bột 180k', N'', N'180000', N'/admin_images/up-img.png', 6)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (18, N'Móng úp 80k', N'', N'80000', N'/admin_images/up-img.png', 6)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (19, N'Đá nhô (1k)', N'', N'1000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (20, N'Đá nhô (2k)', N'', N'2000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (21, N'Đá nhô (3k)', N'', N'3000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (22, N'Đá nhô (4k)', N'', N'4000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (23, N'Đá nhô (5k)', N'', N'5000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (24, N'Chàm nhỏ (1k)', N'', N'1000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (25, N'Chàm nhỏ (2k)', N'', N'2000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (26, N'Chàm nhỏ (3k)', N'', N'3000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (27, N'Chàm nhỏ (4k)', N'', N'4000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (28, N'Chàm nhỏ (5k)', N'', N'5000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (29, N'Đá hình 15k', N'', N'15000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (30, N'Đá hình 10k', N'', N'10000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (31, N'Đá hình 20k', N'', N'20000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (32, N'Đá khối/đá hình siêu sáng 10k', N'', N'10000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (33, N'Đá khối/đá hình siêu sáng 20k', N'', N'20000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (34, N'Đá khối/đá hình siêu sáng 30k', N'', N'30000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (35, N'Đá khối/đá hình siêu sáng 40k', N'', N'40000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (36, N'Charm 10k', N'', N'10000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (37, N'Charm 20k', N'', N'20000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (38, N'Charm 30k', N'', N'30000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (39, N'Hoa bột 10k', N'', N'10000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (40, N'Hoa bột 20k', N'', N'20000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (41, N'Hoa bột 30k', N'', N'30000', N'/admin_images/up-img.png', 4)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (43, N'Onbre 5k/ngón', N'', N'5000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (44, N'Onbre 10k/ngón', N'', N'10000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (45, N'Vân đá 10k', N'', N'10000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (46, N'Kẻ đầu móng 5k', N'', N'5000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (47, N'Kẻ đầu móng 10k', N'', N'10000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (48, N'Ẩn hoa 10k', N'', N'10000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (49, N'Ẩn hoa 20k', N'', N'20000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (50, N'Xà cừ 10k', N'', N'10000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (51, N'Xà cừ 20k', N'', N'20000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (52, N'Ẩn nhũ 10k', N'', N'10000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (53, N'Ẩn nhũ 20k', N'', N'20000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (54, N'Loang 10k', N'', N'10000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (55, N'Loang 20k', N'', N'20000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (56, N'Dập phôi 5k', N'', N'5000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (57, N'Tráng gương 5k', N'', N'5000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (58, N'Mắt mèo 10k', N'', N'10000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (59, N'Vẽ Gel (họa tiết, design...) 5k', N'', N'5000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (60, N'Vẽ Gel (họa tiết, design...) 10k', N'', N'10000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (61, N'Vẽ Gel (họa tiết, design...) 15k', N'', N'15000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (62, N'Vẽ Gel (họa tiết, design...) 20k', N'', N'20000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (63, N'Vẽ Gel (họa tiết, design...) 25k', N'', N'25000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (64, N'Vẽ Gel (họa tiết, design...) 30k', N'', N'30000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (65, N'Vẽ Gel (họa tiết, design...) 35k', N'', N'35000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (66, N'Đắp nhũ 10k', N'', N'10000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (67, N'Đắp nhũ 20k', N'', N'20000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (68, N'Phụ kiện (sticker,hình dán nước...) 10k', N'', N'10000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (69, N'Phụ kiện (sticker,hình dán nước...) 20k', N'', N'20000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (70, N'Phá Gel 10k', N'', N'10000', NULL, 1)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (71, N'Phá Gel 20k', N'', N'20000', NULL, 1)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (72, N'Rửa mặt 15k', N'', N'15000', NULL, 3)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (73, N'Sấy tạo kiểu 20k', N'', N'20000', NULL, 3)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (74, N'Tạo kiểu 10k', N'', N'10000', NULL, 3)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (75, N'Nhũ 5k', N'', N'5000', NULL, 2)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (76, N'Đắp mặt nạ 35k', N'', N'35000', NULL, 3)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (77, N'Úp ngón 10k', N'', N'10000', NULL, 6)
INSERT [dbo].[tbDichVu] ([dichvu_id], [dichvu_title], [dichvu_content], [dichvu_price], [dichvu_image], [dvcate_id]) VALUES (78, N'Gội đầu 30k', N'', N'30000', NULL, 3)
SET IDENTITY_INSERT [dbo].[tbDichVu] OFF
SET IDENTITY_INSERT [dbo].[tbHoaDonBanHang] ON 

INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (37, N'HD00007', N'khách hàng a', NULL, N'20', NULL, CAST(N'2020-11-14 16:50:14.697' AS DateTime), 1, NULL, 1, NULL, N'2', NULL, NULL, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (38, N'HD00008', N'bhasgd', NULL, NULL, NULL, CAST(N'2020-11-14 16:52:04.043' AS DateTime), 1, NULL, 1, 15, NULL, NULL, NULL, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (47, N'HD00009', N'Kim Tú', N'250000', N'20', N'200000', CAST(N'2020-11-14 18:20:23.887' AS DateTime), 2, N'50000', 1, NULL, N'2', CAST(N'2020-11-14 18:20:54.513' AS DateTime), 1, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (48, N'HD00010', N'Kim Tú', N'290000', N'20', N'232000', CAST(N'2020-11-14 18:22:44.483' AS DateTime), 2, N'58000', 1, NULL, N'2', CAST(N'2020-11-14 18:23:19.390' AS DateTime), 1, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (49, N'HD00011', N'Khách hàng 1', N'100000', N'10', N'90000', CAST(N'2020-11-14 19:07:25.153' AS DateTime), 1, N'10000', 0, 1, N'1', CAST(N'2020-11-14 19:07:58.783' AS DateTime), 1, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (50, N'HD00012', N'0919698094', N'40000', N'0', N'40000', CAST(N'2020-11-15 14:49:28.153' AS DateTime), 1, N'0', 0, NULL, N'', CAST(N'2020-11-15 14:51:36.767' AS DateTime), 1, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (51, N'HD00013', N'0919698094', N'13000', N'0', N'13000', CAST(N'2020-11-15 14:54:04.050' AS DateTime), 2, N'0', 0, NULL, N'', CAST(N'2020-11-15 14:55:01.597' AS DateTime), 1, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (52, N'HD00014', N'0919698094', N'30000', N'10', N'27000', CAST(N'2020-11-15 14:56:49.147' AS DateTime), 1, N'3000', 0, NULL, N'1', CAST(N'2020-11-15 15:27:10.653' AS DateTime), 1, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (53, N'HD00015', N'acs', N'30000', N'0', N'30000', CAST(N'2020-11-15 15:30:35.890' AS DateTime), 2, N'0', 0, NULL, N'', CAST(N'2020-11-15 15:30:39.560' AS DateTime), 1, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (54, N'HD00016', N'Kim Tú', N'310000', N'0', N'310000', CAST(N'2020-11-15 15:47:59.237' AS DateTime), 2, N'0', 0, NULL, N'', CAST(N'2020-11-15 15:48:03.737' AS DateTime), 1, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (55, N'HD00017', N'Kim Tú', N'120000', N'10', N'108000', CAST(N'2020-11-15 16:02:17.773' AS DateTime), 2, N'12000', 0, NULL, N'1', CAST(N'2020-11-15 16:02:40.037' AS DateTime), 1, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (56, N'HD00018', N'0919698094', N'20000', N'0', N'20000', CAST(N'2020-11-15 16:04:43.010' AS DateTime), 2, N'0', 0, NULL, N'', CAST(N'2020-11-15 16:04:55.510' AS DateTime), 1, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (58, N'HD00019', N'Khánh Chi', N'121000', N'20', N'96800', CAST(N'2020-11-15 16:07:44.137' AS DateTime), 2, N'24200', 0, 16, N'2', CAST(N'2020-11-15 16:11:01.657' AS DateTime), 1, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (59, N'HD00020', N'Khánh Chi', NULL, N'20', NULL, CAST(N'2020-11-15 16:15:45.257' AS DateTime), 2, NULL, 1, NULL, N'2', NULL, NULL, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (61, N'HD00021', N'Khánh Chi', N'168000', N'0', N'168000', CAST(N'2020-11-16 08:59:32.163' AS DateTime), 2, N'0', 0, NULL, N'', CAST(N'2020-11-16 12:25:45.063' AS DateTime), 1, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (64, N'HD00023', N'Khách hàng 1', N'35000', N'20', N'28000', CAST(N'2020-11-16 09:21:28.533' AS DateTime), 2, N'7000', 0, 1, N'2', CAST(N'2020-11-16 09:21:36.610' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (66, N'HD00025', N'Khách hàng 1', N'60000', N'20', N'48000', CAST(N'2020-11-16 09:59:28.120' AS DateTime), 2, N'12000', 0, 1, N'2', CAST(N'2020-11-16 10:12:08.633' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (70, N'HD00029', N'Kiều Thuận', N'196000', N'20', N'156800', CAST(N'2020-11-16 11:48:40.627' AS DateTime), 2, N'39200', 0, 18, N'2', CAST(N'2020-11-16 11:52:08.180' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (71, N'HD00030', N'Khánh Chi', N'168000', N'20', N'134400', CAST(N'2020-11-16 12:27:54.487' AS DateTime), 2, N'33600', 0, NULL, N'2', CAST(N'2020-11-16 12:28:05.380' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (72, N'HD00031', N'Lân', N'113000', N'20', N'90400', CAST(N'2020-11-16 12:29:55.130' AS DateTime), 2, N'22600', 0, NULL, N'2', CAST(N'2020-11-16 12:30:06.397' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (74, N'HD00033', N'Khách hàng 2', N'99000', N'20', N'79200', CAST(N'2020-11-16 12:52:40.960' AS DateTime), 2, N'19800', 0, 2, N'2', CAST(N'2020-11-16 12:52:47.287' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (76, N'HD00035', N'Khách hàng 2', N'326000', N'20', N'260800', CAST(N'2020-11-16 13:59:49.130' AS DateTime), 2, N'65200', 0, 2, N'2', CAST(N'2020-11-16 15:04:43.490' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (78, N'HD00037', N'Khách hàng 4', N'178000', N'20', N'142400', CAST(N'2020-11-16 14:14:42.837' AS DateTime), 2, N'35600', 0, 4, N'2', CAST(N'2020-11-16 16:04:36.840' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (79, N'HD00038', N'Khách hàng 5', N'89000', N'20', N'71200', CAST(N'2020-11-16 14:27:14.303' AS DateTime), 2, N'17800', 0, 5, N'2', CAST(N'2020-11-16 15:31:35.427' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (80, N'HD00039', N'Khách hàng 1', N'40000', N'20', N'32000', CAST(N'2020-11-17 08:24:37.993' AS DateTime), 2, N'8000', 0, 1, N'2', CAST(N'2020-11-17 08:25:03.853' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (81, N'HD00040', N'Tiên Tiên', N'120000', N'20', N'96000', CAST(N'2020-11-17 11:09:44.983' AS DateTime), 2, N'24000', 0, 19, N'2', CAST(N'2020-11-17 11:16:08.257' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (82, N'HD00041', N'Huệ', N'89000', N'20', N'71200', CAST(N'2020-11-17 11:16:57.680' AS DateTime), 2, N'17800', 0, 20, N'2', CAST(N'2020-11-17 11:17:48.963' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (83, N'HD00042', N'Khách hàng 1', N'119000', N'20', N'95200', CAST(N'2020-11-17 12:39:57.653' AS DateTime), 2, N'23800', 0, 1, N'2', CAST(N'2020-11-17 13:48:30.643' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (85, N'HD00043', N'Khách hàng 2', N'109000', N'20', N'87200', CAST(N'2020-11-17 13:32:41.997' AS DateTime), 2, N'21800', 0, 2, N'2', CAST(N'2020-11-17 14:36:03.503' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (86, N'HD00044', N'Khách hàng 1', NULL, N'0', NULL, CAST(N'2020-11-17 16:11:54.227' AS DateTime), 2, NULL, 1, 1, N'', NULL, NULL, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (87, N'HD00045', N'Khách hàng 1', N'218000', N'20', N'174400', CAST(N'2020-11-18 08:27:46.260' AS DateTime), 2, N'43600', 0, 1, N'2', CAST(N'2020-11-18 08:30:25.713' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (88, N'HD00046', N'Khách hàng 1', N'129000', N'20', N'103200', CAST(N'2020-11-18 08:30:35.870' AS DateTime), 2, N'25800', 0, 1, N'2', CAST(N'2020-11-18 08:32:44.687' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (90, N'HD00047', N'Khách hàng 1', N'316000', N'20', N'252800', CAST(N'2020-11-18 08:32:50.280' AS DateTime), 2, N'63200', 0, 1, N'2', CAST(N'2020-11-18 08:34:51.940' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (92, N'HD00048', N'Hiền', N'179000', N'20', N'143200', CAST(N'2020-11-18 09:39:55.500' AS DateTime), 2, N'35800', 0, NULL, N'2', CAST(N'2020-11-18 11:50:52.310' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (95, N'HD00049', N'Ái Quỳnh', N'139000', N'20', N'111200', CAST(N'2020-11-18 14:57:38.333' AS DateTime), 2, N'27800', 0, 21, N'2', CAST(N'2020-11-18 15:42:51.263' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (96, N'HD00050', N'Dung', N'35000', N'20', N'28000', CAST(N'2020-11-18 14:59:01.053' AS DateTime), 2, N'7000', 0, NULL, N'2', CAST(N'2020-11-18 15:36:25.380' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (97, N'HD00051', N'Khách hàng 1', N'80000', N'20', N'64000', CAST(N'2020-11-19 08:37:56.773' AS DateTime), 2, N'16000', 0, 1, N'2', CAST(N'2020-11-19 08:39:48.883' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (98, N'HD00052', N'Khách hàng 1', N'50000', N'20', N'40000', CAST(N'2020-11-19 08:39:53.430' AS DateTime), 2, N'10000', 0, 1, N'2', CAST(N'2020-11-19 08:40:30.870' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (99, N'HD00053', N'Khách hàng 1', N'228000', N'20', N'182400', CAST(N'2020-11-19 14:25:03.950' AS DateTime), 2, N'45600', 0, 1, N'2', CAST(N'2020-11-19 14:30:26.207' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (100, N'HD00054', N'Khách hàng 1', N'159000', N'20', N'127200', CAST(N'2020-11-19 14:25:34.843' AS DateTime), 2, N'31800', 0, NULL, N'2', CAST(N'2020-11-19 16:33:14.047' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (101, N'HD00055', N'Khách hàng 2', N'104000', N'20', N'83200', CAST(N'2020-11-19 15:53:01.390' AS DateTime), 2, N'20800', 0, 2, N'2', CAST(N'2020-11-19 16:29:12.010' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (103, N'HD00056', N'Khách hàng 2', NULL, N'20', NULL, CAST(N'2020-11-19 16:29:36.277' AS DateTime), 2, NULL, 1, 2, N'2', NULL, NULL, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (104, N'HD00057', N'Khách hàng 1', N'179000', N'20', N'143200', CAST(N'2020-11-20 08:32:34.733' AS DateTime), 2, N'35800', 0, 1, N'2', CAST(N'2020-11-20 08:33:38.267' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (105, N'HD00058', N'Khách hàng 1', N'20000', N'20', N'16000', CAST(N'2020-11-20 08:54:45.727' AS DateTime), 2, N'4000', 0, 1, N'2', CAST(N'2020-11-20 08:55:06.633' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (109, N'HD00060', N'Minh Anh', N'418000', N'20', N'334400', CAST(N'2020-11-20 15:27:15.273' AS DateTime), 2, N'83600', 0, NULL, N'2', CAST(N'2020-11-20 16:22:48.717' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (111, N'HD00061', N'Cô Cơ', N'20000', N'0', N'20000', CAST(N'2020-11-20 15:31:21.543' AS DateTime), 2, N'0', 0, NULL, N'', CAST(N'2020-11-20 15:31:29.810' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (113, N'HD00062', N'Khách hàng 1', N'139000', N'20', N'111200', CAST(N'2020-11-20 16:36:50.437' AS DateTime), 2, N'27800', 0, 1, N'2', CAST(N'2020-11-20 16:37:57.347' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (114, N'HD00063', N'Khách hàng 2', N'139000', N'20', N'111200', CAST(N'2020-11-20 16:37:21.987' AS DateTime), 2, N'27800', 0, 2, N'2', CAST(N'2020-11-20 16:38:24.110' AS DateTime), 1, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (115, N'HD00064', N'Khách hàng 1', N'20000', N'20', N'16000', CAST(N'2020-11-20 16:39:30.550' AS DateTime), 2, N'4000', 0, 1, N'2', CAST(N'2020-11-20 16:39:51.333' AS DateTime), 0, 2)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (117, N'HD00066', N'Khách hàng 1', NULL, N'0', NULL, CAST(N'2020-11-20 19:44:31.737' AS DateTime), 2, NULL, 1, 1, N'', NULL, NULL, 5)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (121, N'HD00067', N'Khách hàng 1', N'59000', N'20', N'47200', CAST(N'2020-11-21 08:33:08.070' AS DateTime), 2, N'11800', 0, 1, N'2', CAST(N'2020-11-21 08:33:28.320' AS DateTime), 0, 4)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (122, N'HD00068', N'Khách hàng 1', N'178000', N'20', N'142400', CAST(N'2020-11-21 08:33:35.870' AS DateTime), 2, N'35600', 0, 1, N'2', CAST(N'2020-11-21 08:34:51.573' AS DateTime), 0, 4)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (123, N'HD00069', N'Khách hàng 1', N'178000', N'20', N'142400', CAST(N'2020-11-21 08:34:41.540' AS DateTime), 2, N'35600', 0, NULL, N'2', CAST(N'2020-11-21 08:35:34.213' AS DateTime), 0, 5)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (125, N'HD00070', N'Khách hàng 1', N'193000', N'20', N'154400', CAST(N'2020-11-21 08:35:39.213' AS DateTime), 2, N'38600', 0, 1, N'2', CAST(N'2020-11-21 08:36:56.340' AS DateTime), 0, 5)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (126, N'HD00071', N'Cô Cơ', N'20000', N'0', N'20000', CAST(N'2020-11-21 11:56:00.407' AS DateTime), 2, N'0', 0, NULL, N'', CAST(N'2020-11-21 11:56:26.797' AS DateTime), 1, 5)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (127, N'HD00072', N'Cô Cơ', N'20000', N'0', N'20000', CAST(N'2020-11-21 11:57:10.720' AS DateTime), 2, N'0', 0, NULL, N'', CAST(N'2020-11-21 11:57:26.253' AS DateTime), 1, 5)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (128, N'HD00073', N'Cô Cơ', N'20000', N'20', N'16000', CAST(N'2020-11-21 11:57:54.003' AS DateTime), 2, N'4000', 0, NULL, N'2', CAST(N'2020-11-21 11:58:13.003' AS DateTime), 0, 5)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (132, N'HD00074', N'Vĩnh', N'441000', N'20', N'352800', CAST(N'2020-11-21 12:35:29.030' AS DateTime), 2, N'88200', 0, NULL, N'2', CAST(N'2020-11-21 14:12:08.663' AS DateTime), 0, 5)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (138, N'HD00076', N'Khách hàng 2', N'55000', N'20', N'44000', CAST(N'2020-11-21 18:00:50.323' AS DateTime), 2, N'11000', 0, 2, N'2', CAST(N'2020-11-21 18:11:27.163' AS DateTime), 0, 5)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (139, N'HD00077', N'Khách hàng 3', N'35000', N'20', N'28000', CAST(N'2020-11-21 18:01:23.307' AS DateTime), 2, N'7000', 0, 3, N'2', CAST(N'2020-11-21 18:11:45.820' AS DateTime), 0, 5)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (140, N'HD00078', N'Khách hàng 4', N'79000', N'20', N'63200', CAST(N'2020-11-21 18:01:58.870' AS DateTime), 2, N'15800', 0, 4, N'2', CAST(N'2020-11-21 18:31:16.907' AS DateTime), 0, 5)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (141, N'HD00079', N'Khách hàng 1', N'20000', N'20', N'16000', CAST(N'2020-11-21 18:04:25.280' AS DateTime), 2, N'4000', 0, 1, N'2', CAST(N'2020-11-21 18:31:23.220' AS DateTime), 0, 5)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (142, N'HD00080', N'Khách hàng 1', N'20000', N'0', N'20000', CAST(N'2020-11-22 08:25:35.047' AS DateTime), 2, N'0', 0, 1, N'', CAST(N'2020-11-22 08:25:47.000' AS DateTime), 0, 5)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (144, N'HD00081', N'Khách hàng 1', N'55000', N'20', N'44000', CAST(N'2020-11-22 12:53:50.397' AS DateTime), 2, N'11000', 0, 1, N'2', CAST(N'2020-11-22 12:54:27.023' AS DateTime), 0, 6)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (145, N'HD00082', N'Khách hàng 1', N'99000', N'20', N'79200', CAST(N'2020-11-22 15:06:27.767' AS DateTime), 2, N'19800', 0, 1, N'2', CAST(N'2020-11-22 15:11:35.697' AS DateTime), 0, 6)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (148, N'HD00083', N'Khách hàng 1', N'113000', N'20', N'90400', CAST(N'2020-11-22 16:31:03.770' AS DateTime), 2, N'22600', 0, 1, N'2', CAST(N'2020-11-22 17:30:52.813' AS DateTime), 0, 7)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (149, N'HD00084', N'Khách hàng 2', N'30000', N'20', N'24000', CAST(N'2020-11-22 16:34:04.540' AS DateTime), 2, N'6000', 0, 2, N'2', CAST(N'2020-11-22 17:08:09.533' AS DateTime), 1, 6)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (150, N'HD00085', N'Khách hàng 2', N'70000', N'20', N'56000', CAST(N'2020-11-22 17:08:20.567' AS DateTime), 2, N'14000', 0, 2, N'2', CAST(N'2020-11-22 17:08:39.520' AS DateTime), 0, 6)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (152, N'HD00086', N'Cô Cơ', N'50000', N'0', N'50000', CAST(N'2020-11-23 08:32:21.090' AS DateTime), NULL, N'0', 0, NULL, N'', CAST(N'2020-11-23 08:33:20.750' AS DateTime), 0, 5)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (155, N'HD00088', N'Bích Vân', N'139000', N'0', N'139000', CAST(N'2020-11-23 15:21:26.020' AS DateTime), 2, N'0', 0, 26, N'', CAST(N'2020-11-23 15:47:57.237' AS DateTime), 0, 7)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (156, N'HD00089', N'Khách hàng 1', N'40000', N'0', N'40000', CAST(N'2020-11-23 15:21:31.677' AS DateTime), 2, N'0', 0, 1, N'', CAST(N'2020-11-23 15:21:50.240' AS DateTime), 0, 7)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (157, N'HD00090', N'Khách hàng 1', N'30000', N'0', N'30000', CAST(N'2020-11-23 16:05:27.913' AS DateTime), 2, N'0', 0, 1, N'', CAST(N'2020-11-23 16:20:00.433' AS DateTime), 0, 7)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (158, N'HD00091', N'Quốc Bảo', NULL, NULL, NULL, CAST(N'2020-11-23 19:22:33.050' AS DateTime), 2, NULL, 1, 27, NULL, NULL, NULL, NULL)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (159, N'HD00092', N'Duyên', N'218000', NULL, N'218000', CAST(N'2020-11-24 11:33:06.030' AS DateTime), 2, N'0', 0, 28, NULL, CAST(N'2020-11-24 11:51:32.147' AS DateTime), 0, 5)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (164, N'HD00095', N'Khách hàng 1', N'15000', N'0', N'15000', CAST(N'2020-11-25 15:55:12.177' AS DateTime), 2, N'0', 0, 1, N'', CAST(N'2020-11-25 15:55:31.630' AS DateTime), 0, 6)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (165, N'HD00096', N'Khách hàng 1', N'40000', N'0', N'40000', CAST(N'2020-11-25 15:55:41.770' AS DateTime), 2, N'0', 0, 1, N'', CAST(N'2020-11-25 15:56:10.303' AS DateTime), 0, 6)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (166, N'HD00097', N'Khách hàng 1', N'124000', N'0', N'124000', CAST(N'2020-11-25 15:56:22.350' AS DateTime), 2, N'0', 0, 1, N'', CAST(N'2020-11-25 15:57:07.070' AS DateTime), 0, 6)
INSERT [dbo].[tbHoaDonBanHang] ([hoadon_id], [hoadon_code], [khachhang_name], [hoadon_tongtien], [hoadon_giamgia], [hoadon_phaitra], [hoadon_createdate], [username_id], [hoadon_tongtiengiam], [active], [khachhang_id], [khuyenmai_id], [hoadon_giothanhtoan], [hidden], [nhanvien_id]) VALUES (167, N'HD00098', N'Khách hàng 1', N'194000', N'0', N'194000', CAST(N'2020-11-25 15:57:12.100' AS DateTime), 2, N'0', 0, 1, N'', CAST(N'2020-11-25 15:59:01.743' AS DateTime), 0, 6)
SET IDENTITY_INSERT [dbo].[tbHoaDonBanHang] OFF
SET IDENTITY_INSERT [dbo].[tbHoaDonBanHangChiTiet] ON 

INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (78, 28, 4, NULL, 0, CAST(N'2020-11-14 13:54:32.560' AS DateTime), 2, N'200000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (79, 28, 1, NULL, 0, CAST(N'2020-11-14 13:54:32.560' AS DateTime), 2, N'150000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (80, 28, 2, NULL, 7, CAST(N'2020-11-14 13:54:32.560' AS DateTime), 2, N'200000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (81, 28, 3, NULL, 2, CAST(N'2020-11-14 13:54:32.560' AS DateTime), 2, N'180000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (82, 24, 1, NULL, 2, CAST(N'2020-11-14 13:56:36.550' AS DateTime), 1, N'150000', 0, N'10', N'1', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (83, 24, 2, NULL, 1, CAST(N'2020-11-14 13:56:36.647' AS DateTime), 1, N'200000', 0, N'10', N'1', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (84, 24, 4, NULL, 1, CAST(N'2020-11-14 13:56:36.697' AS DateTime), 1, N'200000', 0, N'10', N'1', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (87, 29, 4, NULL, 1, CAST(N'2020-11-14 13:57:26.140' AS DateTime), 2, N'200000', 0, N'0', N'', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (88, 29, 2, NULL, 1, CAST(N'2020-11-14 13:57:26.140' AS DateTime), 2, N'200000', 0, N'0', N'', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (91, 30, 4, NULL, 1, CAST(N'2020-11-14 14:06:51.713' AS DateTime), 2, N'200000', 0, NULL, NULL, NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (92, 30, 2, NULL, 1, CAST(N'2020-11-14 14:06:51.713' AS DateTime), 2, N'200000', 0, NULL, NULL, NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (97, 31, 4, NULL, 2, CAST(N'2020-11-14 14:07:53.607' AS DateTime), 2, N'200000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (98, 31, 3, NULL, 2, CAST(N'2020-11-14 14:07:53.607' AS DateTime), 2, N'180000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (99, 31, 2, NULL, 2, CAST(N'2020-11-14 14:07:53.607' AS DateTime), 2, N'200000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (103, 37, 7, NULL, 2, CAST(N'2020-11-14 16:50:39.083' AS DateTime), 1, N'50000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (104, 37, 14, NULL, 2, CAST(N'2020-11-14 16:50:46.990' AS DateTime), 1, N'70000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (107, 32, 1, NULL, 3, CAST(N'2020-11-14 16:54:50.083' AS DateTime), 2, N'300000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (108, 32, 3, NULL, 3, CAST(N'2020-11-14 16:54:50.100' AS DateTime), 2, N'30000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (109, 32, 2, NULL, 2, CAST(N'2020-11-14 16:54:50.100' AS DateTime), 2, N'500000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (125, 47, 6, NULL, 1, CAST(N'2020-11-14 18:20:54.497' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (126, 47, 17, NULL, 1, CAST(N'2020-11-14 18:20:54.497' AS DateTime), 2, N'180000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (127, 47, 49, NULL, 1, CAST(N'2020-11-14 18:20:54.497' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (128, 47, 21, NULL, 10, CAST(N'2020-11-14 18:20:54.513' AS DateTime), 2, N'3000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (133, 48, 7, NULL, 1, CAST(N'2020-11-14 18:23:19.390' AS DateTime), 2, N'50000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (134, 48, 17, NULL, 1, CAST(N'2020-11-14 18:23:19.390' AS DateTime), 2, N'180000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (135, 48, 69, NULL, 1, CAST(N'2020-11-14 18:23:19.390' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (136, 48, 22, NULL, 10, CAST(N'2020-11-14 18:23:19.390' AS DateTime), 2, N'4000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (139, 49, 71, NULL, 2, CAST(N'2020-11-14 19:07:58.673' AS DateTime), 1, N'20000', 0, N'10', N'1', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (140, 49, 3, NULL, 2, CAST(N'2020-11-14 19:07:58.727' AS DateTime), 1, N'30000', 0, N'10', N'1', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (142, 50, 8, NULL, 1, CAST(N'2020-11-15 14:51:36.750' AS DateTime), 1, N'40000', 0, N'0', N'', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (145, 51, 26, NULL, 1, CAST(N'2020-11-15 14:55:01.567' AS DateTime), 2, N'3000', 0, N'0', N'', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (146, 51, 32, NULL, 1, CAST(N'2020-11-15 14:55:01.580' AS DateTime), 2, N'10000', 0, N'0', N'', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (149, 52, 48, NULL, 1, CAST(N'2020-11-15 15:27:10.637' AS DateTime), 2, N'10000', 0, N'10', N'1', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (150, 52, 53, NULL, 1, CAST(N'2020-11-15 15:27:10.637' AS DateTime), 2, N'20000', 0, N'10', N'1', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (153, 53, 70, NULL, 1, CAST(N'2020-11-15 15:30:39.560' AS DateTime), 2, N'10000', 0, N'0', N'', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (154, 53, 71, NULL, 1, CAST(N'2020-11-15 15:30:39.560' AS DateTime), 2, N'20000', 0, N'0', N'', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (157, 54, 70, NULL, 1, CAST(N'2020-11-15 15:48:03.723' AS DateTime), 2, N'10000', 0, N'0', N'', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (158, 54, 1, NULL, 1, CAST(N'2020-11-15 15:48:03.723' AS DateTime), 2, N'300000', 0, N'0', N'', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (162, 55, 70, NULL, 2, CAST(N'2020-11-15 16:02:40.037' AS DateTime), 2, N'10000', 0, N'10', N'1', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (163, 55, 71, NULL, 2, CAST(N'2020-11-15 16:02:40.037' AS DateTime), 2, N'20000', 0, N'10', N'1', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (164, 55, 3, NULL, 2, CAST(N'2020-11-15 16:02:40.037' AS DateTime), 2, N'30000', 0, N'10', N'1', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (166, 56, 71, NULL, 1, CAST(N'2020-11-15 16:04:55.510' AS DateTime), 2, N'20000', 0, N'0', N'', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (171, 58, 4, NULL, 2, CAST(N'2020-11-15 16:11:01.610' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (172, 58, 19, NULL, 2, CAST(N'2020-11-15 16:11:01.623' AS DateTime), 2, N'1000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (173, 58, 58, NULL, 2, CAST(N'2020-11-15 16:11:01.640' AS DateTime), 2, N'10000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (174, 58, 11, NULL, 1, CAST(N'2020-11-15 16:11:01.640' AS DateTime), 2, N'59000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (175, 59, 4, NULL, 2, CAST(N'2020-11-15 16:15:54.367' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (176, 59, 11, NULL, 2, CAST(N'2020-11-15 16:16:03.837' AS DateTime), 2, N'59000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (177, 59, 19, NULL, 2, CAST(N'2020-11-15 16:16:14.820' AS DateTime), 2, N'1000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (178, 59, 58, NULL, 2, CAST(N'2020-11-15 16:17:20.103' AS DateTime), 2, N'10000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (184, 64, 72, NULL, 1, CAST(N'2020-11-16 09:21:36.610' AS DateTime), 2, N'15000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (185, 64, 6, NULL, 1, CAST(N'2020-11-16 09:21:36.610' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (193, 66, 7, NULL, 1, CAST(N'2020-11-16 10:12:08.633' AS DateTime), 2, N'50000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (194, 66, 74, NULL, 1, CAST(N'2020-11-16 10:12:08.633' AS DateTime), 2, N'10000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (208, 70, 19, NULL, 8, CAST(N'2020-11-16 11:52:08.163' AS DateTime), 2, N'1000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (209, 70, 4, NULL, 2, CAST(N'2020-11-16 11:52:08.163' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (210, 70, 11, NULL, 2, CAST(N'2020-11-16 11:52:08.180' AS DateTime), 2, N'59000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (211, 70, 75, NULL, 2, CAST(N'2020-11-16 11:52:08.180' AS DateTime), 2, N'5000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (212, 70, 48, NULL, 2, CAST(N'2020-11-16 11:52:08.180' AS DateTime), 2, N'10000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (213, 61, 11, NULL, 2, CAST(N'2020-11-16 12:25:45.047' AS DateTime), 2, N'59000', 0, N'0', N'', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (214, 61, 4, NULL, 2, CAST(N'2020-11-16 12:25:45.047' AS DateTime), 2, N'20000', 0, N'0', N'', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (215, 61, 19, NULL, 10, CAST(N'2020-11-16 12:25:45.047' AS DateTime), 2, N'1000', 0, N'0', N'', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (219, 71, 4, NULL, 2, CAST(N'2020-11-16 12:28:05.363' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (220, 71, 11, NULL, 2, CAST(N'2020-11-16 12:28:05.363' AS DateTime), 2, N'59000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (221, 71, 19, NULL, 10, CAST(N'2020-11-16 12:28:05.363' AS DateTime), 2, N'1000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (226, 72, 6, NULL, 1, CAST(N'2020-11-16 12:30:06.380' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (227, 72, 11, NULL, 1, CAST(N'2020-11-16 12:30:06.380' AS DateTime), 2, N'59000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (228, 72, 20, NULL, 7, CAST(N'2020-11-16 12:30:06.380' AS DateTime), 2, N'2000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (229, 72, 60, NULL, 2, CAST(N'2020-11-16 12:30:06.397' AS DateTime), 2, N'10000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (237, 74, 11, NULL, 1, CAST(N'2020-11-16 12:52:47.287' AS DateTime), 2, N'59000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (238, 74, 4, NULL, 1, CAST(N'2020-11-16 12:52:47.287' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (239, 74, 60, NULL, 2, CAST(N'2020-11-16 12:52:47.287' AS DateTime), 2, N'10000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (255, 76, 18, NULL, 1, CAST(N'2020-11-16 15:04:43.473' AS DateTime), 2, N'80000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (256, 76, 11, NULL, 4, CAST(N'2020-11-16 15:04:43.473' AS DateTime), 2, N'59000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (257, 76, 54, NULL, 1, CAST(N'2020-11-16 15:04:43.473' AS DateTime), 2, N'10000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (259, 79, 4, NULL, 1, CAST(N'2020-11-16 15:31:35.413' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (260, 79, 11, NULL, 1, CAST(N'2020-11-16 15:31:35.413' AS DateTime), 2, N'59000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (261, 79, 57, NULL, 2, CAST(N'2020-11-16 15:31:35.427' AS DateTime), 2, N'5000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (263, 78, 4, NULL, 2, CAST(N'2020-11-16 16:04:36.827' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (264, 78, 11, NULL, 2, CAST(N'2020-11-16 16:04:36.840' AS DateTime), 2, N'59000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (265, 78, 60, NULL, 2, CAST(N'2020-11-16 16:04:36.840' AS DateTime), 2, N'10000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (267, 80, 6, NULL, 2, CAST(N'2020-11-17 08:25:03.853' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (271, 81, 7, NULL, 1, CAST(N'2020-11-17 11:16:08.240' AS DateTime), 2, N'50000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (272, 81, 8, NULL, 1, CAST(N'2020-11-17 11:16:08.240' AS DateTime), 2, N'40000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (273, 81, 76, NULL, 1, CAST(N'2020-11-17 11:16:08.257' AS DateTime), 2, N'30000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (277, 82, 4, NULL, 1, CAST(N'2020-11-17 11:17:48.963' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (278, 82, 11, NULL, 1, CAST(N'2020-11-17 11:17:48.963' AS DateTime), 2, N'59000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (279, 82, 57, NULL, 2, CAST(N'2020-11-17 11:17:48.963' AS DateTime), 2, N'5000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (286, 83, 4, NULL, 2, CAST(N'2020-11-17 13:48:30.627' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (287, 83, 11, NULL, 1, CAST(N'2020-11-17 13:48:30.643' AS DateTime), 2, N'59000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (288, 83, 77, NULL, 2, CAST(N'2020-11-17 13:48:30.643' AS DateTime), 2, N'10000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (289, 85, 3, NULL, 1, CAST(N'2020-11-17 14:36:03.470' AS DateTime), 2, N'30000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (290, 85, 11, NULL, 1, CAST(N'2020-11-17 14:36:03.487' AS DateTime), 2, N'59000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (291, 85, 52, NULL, 2, CAST(N'2020-11-17 14:36:03.487' AS DateTime), 2, N'10000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (292, 86, 70, NULL, 1, CAST(N'2020-11-17 16:12:04.100' AS DateTime), 2, N'10000', 0, N'0', N'', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (293, 86, 4, NULL, 2, CAST(N'2020-11-17 16:12:08.680' AS DateTime), 2, N'20000', 0, N'0', N'', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (294, 86, 14, NULL, 1, CAST(N'2020-11-17 16:13:08.867' AS DateTime), 2, N'70000', 0, N'0', N'', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (295, 86, 11, NULL, 1, CAST(N'2020-11-17 16:13:31.307' AS DateTime), 2, N'59000', 0, N'0', N'', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (300, 87, 4, NULL, 2, CAST(N'2020-11-18 08:30:25.700' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
GO
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (301, 87, 70, NULL, 1, CAST(N'2020-11-18 08:30:25.713' AS DateTime), 2, N'10000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (302, 87, 11, NULL, 2, CAST(N'2020-11-18 08:30:25.713' AS DateTime), 2, N'59000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (303, 87, 58, NULL, 5, CAST(N'2020-11-18 08:30:25.713' AS DateTime), 2, N'10000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (310, 88, 11, NULL, 1, CAST(N'2020-11-18 08:32:44.670' AS DateTime), 2, N'59000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (311, 88, 43, NULL, 10, CAST(N'2020-11-18 08:32:44.687' AS DateTime), 2, N'5000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (312, 88, 4, NULL, 1, CAST(N'2020-11-18 08:32:44.687' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (315, 90, 4, NULL, 4, CAST(N'2020-11-18 08:34:51.940' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (316, 90, 11, NULL, 4, CAST(N'2020-11-18 08:34:51.940' AS DateTime), 2, N'59000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (327, 92, 11, NULL, 1, CAST(N'2020-11-18 11:50:52.297' AS DateTime), 2, N'59000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (328, 92, 23, NULL, 12, CAST(N'2020-11-18 11:50:52.310' AS DateTime), 2, N'5000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (329, 92, 15, NULL, 1, CAST(N'2020-11-18 11:50:52.310' AS DateTime), 2, N'60000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (336, 96, 6, NULL, 1, CAST(N'2020-11-18 15:36:25.363' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (337, 96, 72, NULL, 1, CAST(N'2020-11-18 15:36:25.363' AS DateTime), 2, N'15000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (339, 95, 4, NULL, 2, CAST(N'2020-11-18 15:42:51.247' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (340, 95, 11, NULL, 1, CAST(N'2020-11-18 15:42:51.263' AS DateTime), 2, N'59000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (341, 95, 51, NULL, 2, CAST(N'2020-11-18 15:42:51.263' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (345, 97, 7, NULL, 1, CAST(N'2020-11-19 08:39:48.883' AS DateTime), 2, N'50000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (346, 97, 76, NULL, 1, CAST(N'2020-11-19 08:39:48.883' AS DateTime), 2, N'30000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (348, 98, 7, NULL, 1, CAST(N'2020-11-19 08:40:30.870' AS DateTime), 2, N'50000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (357, 99, 4, NULL, 2, CAST(N'2020-11-19 14:30:26.190' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (358, 99, 11, NULL, 2, CAST(N'2020-11-19 14:30:26.190' AS DateTime), 2, N'59000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (359, 99, 43, NULL, 8, CAST(N'2020-11-19 14:30:26.190' AS DateTime), 2, N'5000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (360, 99, 68, NULL, 2, CAST(N'2020-11-19 14:30:26.190' AS DateTime), 2, N'10000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (361, 99, 59, NULL, 2, CAST(N'2020-11-19 14:30:26.190' AS DateTime), 2, N'5000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (370, 101, 4, NULL, 1, CAST(N'2020-11-19 16:29:12.010' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (371, 101, 11, NULL, 1, CAST(N'2020-11-19 16:29:12.010' AS DateTime), 2, N'59000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (372, 101, 59, NULL, 5, CAST(N'2020-11-19 16:29:12.010' AS DateTime), 2, N'5000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (373, 103, 6, NULL, 1, CAST(N'2020-11-19 16:29:55.167' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (375, 100, 4, NULL, 1, CAST(N'2020-11-19 16:33:14.030' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (376, 100, 11, NULL, 1, CAST(N'2020-11-19 16:33:14.030' AS DateTime), 2, N'59000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (377, 100, 7, NULL, 1, CAST(N'2020-11-19 16:33:14.047' AS DateTime), 2, N'50000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (378, 100, 71, NULL, 1, CAST(N'2020-11-19 16:33:14.047' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (379, 100, 57, NULL, 2, CAST(N'2020-11-19 16:33:14.047' AS DateTime), 2, N'5000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (387, 104, 7, NULL, 1, CAST(N'2020-11-20 08:33:38.250' AS DateTime), 2, N'50000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (388, 104, 4, NULL, 1, CAST(N'2020-11-20 08:33:38.250' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (389, 104, 71, NULL, 1, CAST(N'2020-11-20 08:33:38.250' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (390, 104, 11, NULL, 1, CAST(N'2020-11-20 08:33:38.250' AS DateTime), 2, N'59000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (391, 104, 57, NULL, 2, CAST(N'2020-11-20 08:33:38.267' AS DateTime), 2, N'5000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (392, 104, 6, NULL, 1, CAST(N'2020-11-20 08:33:38.267' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (394, 105, 6, NULL, 1, CAST(N'2020-11-20 08:55:06.633' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (417, 111, 6, NULL, 1, CAST(N'2020-11-20 15:31:29.810' AS DateTime), 2, N'20000', 0, N'0', N'', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (424, 109, 7, NULL, 1, CAST(N'2020-11-20 16:22:48.703' AS DateTime), 2, N'50000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (425, 109, 8, NULL, 1, CAST(N'2020-11-20 16:22:48.703' AS DateTime), 2, N'40000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (426, 109, 18, NULL, 2, CAST(N'2020-11-20 16:22:48.703' AS DateTime), 2, N'80000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (427, 109, 11, NULL, 2, CAST(N'2020-11-20 16:22:48.703' AS DateTime), 2, N'59000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (428, 109, 59, NULL, 10, CAST(N'2020-11-20 16:22:48.717' AS DateTime), 2, N'5000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (431, 113, 18, NULL, 1, CAST(N'2020-11-20 16:37:57.347' AS DateTime), 2, N'80000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (432, 113, 11, NULL, 1, CAST(N'2020-11-20 16:37:57.347' AS DateTime), 2, N'59000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (435, 114, 11, NULL, 1, CAST(N'2020-11-20 16:38:24.110' AS DateTime), 2, N'59000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (436, 114, 18, NULL, 1, CAST(N'2020-11-20 16:38:24.110' AS DateTime), 2, N'80000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (438, 115, 6, NULL, 1, CAST(N'2020-11-20 16:39:51.333' AS DateTime), 2, N'20000', 0, N'20', N'2', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (441, 117, 10, NULL, 1, CAST(N'2020-11-20 19:44:31.737' AS DateTime), 2, N'30000', 0, N'0', N'', 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (442, 117, 9, NULL, 1, CAST(N'2020-11-20 19:44:31.737' AS DateTime), 2, N'25000', 1, N'0', N'', 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (443, 117, 14, NULL, 1, CAST(N'2020-11-20 21:01:59.667' AS DateTime), 1, N'70000', 0, N'0', N'', NULL)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (450, 121, 11, NULL, 1, CAST(N'2020-11-21 08:33:28.320' AS DateTime), 2, N'59000', 0, N'20', N'2', 4)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (454, 122, 4, NULL, 2, CAST(N'2020-11-21 08:34:51.543' AS DateTime), 2, N'20000', 0, N'20', N'2', 4)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (455, 122, 11, NULL, 2, CAST(N'2020-11-21 08:34:51.543' AS DateTime), 2, N'59000', 0, N'20', N'2', 4)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (456, 122, 59, NULL, 4, CAST(N'2020-11-21 08:34:51.557' AS DateTime), 2, N'5000', 0, N'20', N'2', 4)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (460, 123, 4, NULL, 2, CAST(N'2020-11-21 08:35:34.200' AS DateTime), 2, N'20000', 0, N'20', N'2', 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (461, 123, 11, NULL, 2, CAST(N'2020-11-21 08:35:34.213' AS DateTime), 2, N'59000', 0, N'20', N'2', 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (462, 123, 59, NULL, 4, CAST(N'2020-11-21 08:35:34.213' AS DateTime), 2, N'5000', 0, N'20', N'2', 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (467, 125, 4, NULL, 2, CAST(N'2020-11-21 08:36:56.340' AS DateTime), 2, N'20000', 0, N'20', N'2', 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (468, 125, 11, NULL, 2, CAST(N'2020-11-21 08:36:56.340' AS DateTime), 2, N'59000', 0, N'20', N'2', 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (469, 125, 59, NULL, 3, CAST(N'2020-11-21 08:36:56.340' AS DateTime), 2, N'5000', 0, N'20', N'2', 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (470, 125, 6, NULL, 1, CAST(N'2020-11-21 08:36:56.340' AS DateTime), 2, N'20000', 0, N'20', N'2', 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (471, 126, 6, NULL, 1, CAST(N'2020-11-21 11:56:26.797' AS DateTime), 2, N'20000', 0, N'0', N'', 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (472, 127, 6, NULL, 1, CAST(N'2020-11-21 11:57:26.253' AS DateTime), 2, N'20000', 0, N'0', N'', 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (473, 128, 6, NULL, 1, CAST(N'2020-11-21 11:58:12.987' AS DateTime), 2, N'20000', 0, N'20', N'2', 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (487, 132, 4, NULL, 4, CAST(N'2020-11-21 14:12:08.630' AS DateTime), 2, N'20000', 0, N'20', N'2', 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (488, 132, 11, NULL, 4, CAST(N'2020-11-21 14:12:08.647' AS DateTime), 2, N'59000', 0, N'20', N'2', 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (489, 132, 50, NULL, 4, CAST(N'2020-11-21 14:12:08.647' AS DateTime), 2, N'10000', 0, N'20', N'2', 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (490, 132, 58, NULL, 2, CAST(N'2020-11-21 14:12:08.647' AS DateTime), 2, N'10000', 0, N'20', N'2', 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (491, 132, 43, NULL, 10, CAST(N'2020-11-21 14:12:08.647' AS DateTime), 2, N'5000', 0, N'20', N'2', 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (492, 132, 59, NULL, 3, CAST(N'2020-11-21 14:12:08.647' AS DateTime), 2, N'5000', 0, N'20', N'2', 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (500, 138, 76, NULL, 1, CAST(N'2020-11-21 18:11:27.163' AS DateTime), 2, N'35000', 0, N'20', N'2', 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (501, 138, 6, NULL, 1, CAST(N'2020-11-21 18:11:27.163' AS DateTime), 2, N'20000', 0, N'20', N'2', 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (502, 139, 6, NULL, 1, CAST(N'2020-11-21 18:11:45.820' AS DateTime), 2, N'20000', 0, N'20', N'2', 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (503, 139, 72, NULL, 1, CAST(N'2020-11-21 18:11:45.820' AS DateTime), 2, N'15000', 0, N'20', N'2', 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (504, 140, 4, NULL, 1, CAST(N'2020-11-21 18:31:16.907' AS DateTime), 2, N'20000', 0, N'20', N'2', 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (505, 140, 11, NULL, 1, CAST(N'2020-11-21 18:31:16.907' AS DateTime), 2, N'59000', 0, N'20', N'2', 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (506, 141, 6, NULL, 1, CAST(N'2020-11-21 18:31:23.203' AS DateTime), 2, N'20000', 0, N'20', N'2', 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (508, 142, 6, NULL, 1, CAST(N'2020-11-22 08:25:47.000' AS DateTime), 2, N'20000', 0, N'0', N'', 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (511, 144, 6, NULL, 1, CAST(N'2020-11-22 12:54:27.023' AS DateTime), 2, N'20000', 0, N'20', N'2', 6)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (512, 144, 76, NULL, 1, CAST(N'2020-11-22 12:54:27.023' AS DateTime), 2, N'35000', 0, N'20', N'2', 6)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (516, 145, 6, NULL, 1, CAST(N'2020-11-22 15:11:35.680' AS DateTime), 2, N'20000', 0, N'20', N'2', 6)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (517, 145, 4, NULL, 1, CAST(N'2020-11-22 15:11:35.680' AS DateTime), 2, N'20000', 0, N'20', N'2', 6)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (518, 145, 11, NULL, 1, CAST(N'2020-11-22 15:11:35.697' AS DateTime), 2, N'59000', 0, N'20', N'2', 6)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (528, 149, 4, NULL, 1, CAST(N'2020-11-22 17:08:09.533' AS DateTime), 2, N'20000', 0, N'20', N'2', 6)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (529, 149, 70, NULL, 1, CAST(N'2020-11-22 17:08:09.533' AS DateTime), 2, N'10000', 0, N'20', N'2', 6)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (532, 150, 4, NULL, 1, CAST(N'2020-11-22 17:08:39.503' AS DateTime), 2, N'20000', 0, N'20', N'2', 6)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (533, 150, 2, NULL, 1, CAST(N'2020-11-22 17:08:39.503' AS DateTime), 2, N'50000', 0, N'20', N'2', 6)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (535, 148, 4, NULL, 1, CAST(N'2020-11-22 17:30:52.797' AS DateTime), 2, N'20000', 0, N'20', N'2', 7)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (536, 148, 11, NULL, 1, CAST(N'2020-11-22 17:30:52.813' AS DateTime), 2, N'59000', 0, N'20', N'2', 7)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (537, 148, 20, NULL, 14, CAST(N'2020-11-22 17:30:52.813' AS DateTime), 2, N'2000', 0, N'20', N'2', 7)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (538, 148, 21, NULL, 2, CAST(N'2020-11-22 17:30:52.813' AS DateTime), 2, N'3000', 0, N'20', N'2', 7)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (541, 152, 6, NULL, 1, CAST(N'2020-11-23 08:33:20.750' AS DateTime), 2, N'25000', 0, N'0', N'', 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (542, 152, 9, NULL, 1, CAST(N'2020-11-23 08:33:20.750' AS DateTime), 2, N'25000', 0, N'0', N'', 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (553, 156, 6, NULL, 1, CAST(N'2020-11-23 15:21:50.240' AS DateTime), 2, N'25000', 0, N'0', N'', 7)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (554, 156, 72, NULL, 1, CAST(N'2020-11-23 15:21:50.240' AS DateTime), 2, N'15000', 0, N'0', N'', 7)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (555, 155, 6, NULL, 1, CAST(N'2020-11-23 15:47:57.207' AS DateTime), 2, N'25000', 0, N'0', N'', 7)
GO
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (556, 155, 76, NULL, 1, CAST(N'2020-11-23 15:47:57.223' AS DateTime), 2, N'35000', 0, N'0', N'', 7)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (557, 155, 4, NULL, 1, CAST(N'2020-11-23 15:47:57.223' AS DateTime), 2, N'20000', 0, N'0', N'', 7)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (558, 155, 11, NULL, 1, CAST(N'2020-11-23 15:47:57.237' AS DateTime), 2, N'59000', 0, N'0', N'', 7)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (560, 157, 78, NULL, 1, CAST(N'2020-11-23 16:20:00.400' AS DateTime), 2, N'30000', 0, N'0', N'', 7)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (565, 159, 4, NULL, 2, CAST(N'2020-11-24 11:51:32.067' AS DateTime), 2, N'20000', 0, NULL, NULL, 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (566, 159, 11, NULL, 2, CAST(N'2020-11-24 11:51:32.083' AS DateTime), 2, N'59000', 0, NULL, NULL, 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (567, 159, 59, NULL, 4, CAST(N'2020-11-24 11:51:32.097' AS DateTime), 2, N'5000', 0, NULL, NULL, 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (568, 159, 36, NULL, 4, CAST(N'2020-11-24 11:51:32.113' AS DateTime), 2, N'10000', 0, NULL, NULL, 5)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (571, 164, 72, NULL, 1, CAST(N'2020-11-25 15:55:31.630' AS DateTime), 2, N'15000', 0, N'0', N'', 6)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (575, 165, 6, NULL, 1, CAST(N'2020-11-25 15:56:10.287' AS DateTime), 2, N'25000', 0, N'0', N'', 6)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (576, 165, 72, NULL, 1, CAST(N'2020-11-25 15:56:10.303' AS DateTime), 2, N'15000', 0, N'0', N'', 6)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (580, 166, 4, NULL, 2, CAST(N'2020-11-25 15:57:07.053' AS DateTime), 2, N'20000', 0, N'0', N'', 6)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (581, 166, 11, NULL, 1, CAST(N'2020-11-25 15:57:07.070' AS DateTime), 2, N'59000', 0, N'0', N'', 6)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (582, 166, 6, NULL, 1, CAST(N'2020-11-25 15:57:07.070' AS DateTime), 2, N'25000', 0, N'0', N'', 6)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (588, 167, 7, NULL, 1, CAST(N'2020-11-25 15:59:01.727' AS DateTime), 2, N'50000', 0, N'0', N'', 6)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (589, 167, 8, NULL, 1, CAST(N'2020-11-25 15:59:01.727' AS DateTime), 2, N'40000', 0, N'0', N'', 6)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (590, 167, 11, NULL, 1, CAST(N'2020-11-25 15:59:01.743' AS DateTime), 2, N'59000', 0, N'0', N'', 6)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (591, 167, 46, NULL, 2, CAST(N'2020-11-25 15:59:01.743' AS DateTime), 2, N'5000', 0, N'0', N'', 6)
INSERT [dbo].[tbHoaDonBanHangChiTiet] ([hdct_id], [hoadon_id], [dichvu_id], [product_id], [hdct_soluong], [hdct_createdate], [username_id], [hdct_price], [hidden], [hdct_giamgia], [khuyenmai_id], [nhanvien_id]) VALUES (592, 167, 76, NULL, 1, CAST(N'2020-11-25 15:59:01.743' AS DateTime), 2, N'35000', 0, N'0', N'', 6)
SET IDENTITY_INSERT [dbo].[tbHoaDonBanHangChiTiet] OFF
SET IDENTITY_INSERT [dbo].[tbIntroduce] ON 

INSERT [dbo].[tbIntroduce] ([introduct_id], [introduce_title], [introduce_summary], [introduce_content], [introduce_image], [introduce_createdate], [introduce_update_date]) VALUES (1, N'NICENAILS', N'Các nhà mạng Việt Nam có thể tắt sóng 2G khi số lượng thuê bao sử dụng công nghệ này còn dưới 5%, mục tiêu dự kiến vào năm 2022.', N'<p class="Normal" style="margin: 0px 0px 1em; box-sizing: border-box; text-rendering: optimizespeed; line-height: 28.8px; color: #222222; font-family: arial; font-size: 18px; text-decoration-style: initial; text-decoration-color: initial;"><span style="background-color: #ffffff;">Hiện tại, theo số liệu của Cục Viễn thông, Việt Nam vẫn còn khoảng 24 triệu thuê bao 2G trên tổng số 130 triệu thuê bao đi động. Tuy nhiên, 2G là xu thế đã thoái trào và ngày càng bị thay thế bởi các công nghệ tiên tiến hơn.</span></p>', N'/uploadimages/anh_gioithieu/02112020_100532_SA_messager.png', NULL, CAST(N'2020-11-14 11:49:30.953' AS DateTime))
SET IDENTITY_INSERT [dbo].[tbIntroduce] OFF
SET IDENTITY_INSERT [dbo].[tbNewCate] ON 

INSERT [dbo].[tbNewCate] ([newcate_id], [newcate_title], [newcate_summary], [hidden], [link_seo]) VALUES (1, N'Tin hôm nay', NULL, 0, NULL)
INSERT [dbo].[tbNewCate] ([newcate_id], [newcate_title], [newcate_summary], [hidden], [link_seo]) VALUES (2, N'Tin mới', NULL, 1, NULL)
INSERT [dbo].[tbNewCate] ([newcate_id], [newcate_title], [newcate_summary], [hidden], [link_seo]) VALUES (3, N'Tin nổi bật', NULL, 0, NULL)
SET IDENTITY_INSERT [dbo].[tbNewCate] OFF
SET IDENTITY_INSERT [dbo].[tbNews] ON 

INSERT [dbo].[tbNews] ([news_id], [news_title], [news_summary], [news_image], [news_content], [newcate_id], [hidden], [active], [link_seo], [news_createdate], [news_position]) VALUES (1, N'Bản tin ngày 01/11/2020', N'Cơn mưa lớn kém giông tối 31/10 khiến trường THPT Bình Phú trên đường Trần Văn Kiểu (phường 10, quận 6) bị hư hỏng một dãy nhà. "Lúc đó gió lớn lắm, mái tôn bị kéo sụp xuống đất rất nhanh, nhiều tiếng xẹt điện phát ra", ông Nguyễn Văn Tám, bảo vệ trường nói.', N'/uploadimages/anh_tintuc/fnpnbpxq.l14.png', N'<span style="color: #222222; font-family: arial; font-size: 18px; background-color: #fcfaf6; text-decoration-style: initial; text-decoration-color: initial;">Thống kê của Reuters cho thấy Mỹ hôm 31/10 ghi nhận thêm 100.233 ca nhiễm nCoV chỉ trong 24 giờ, vượt qua kỷ lục 91.295 trường hợp được ghi nhận một ngày trước đó. Đây cũng là mức tăng cao nhất thế giới, vượt qua con số 97.894 ca trong 24 giờ được ghi nhận tại Ấn Độ hồi giữa tháng 9.</span>&nbsp;', 1, 0, 1, NULL, CAST(N'2020-11-01 15:41:09.603' AS DateTime), NULL)
INSERT [dbo].[tbNews] ([news_id], [news_title], [news_summary], [news_image], [news_content], [newcate_id], [hidden], [active], [link_seo], [news_createdate], [news_position]) VALUES (2, N'Mỹ ghi nhận kỷ lục hơn 100.000 ca nCoV một ngày', N'Mỹ báo cáo mức tăng ca nhiễm nCoV trong 24 giờ cao chưa từng có khi chỉ còn vài ngày trước bầu cử tổng thống.', N'/uploadimages/anh_tintuc/hlpm2ktf.reo.png', N'<p class="description" style="margin: 0px 0px 15px; box-sizing: border-box; text-rendering: optimizelegibility; font-size: 18px; line-height: 28.8px; color: #222222; font-family: arial; text-decoration-style: initial; text-decoration-color: initial;"><span style="background-color: #ffffff;">Mỹ báo cáo mức tăng ca nhiễm nCoV trong 24 giờ cao chưa từng có khi chỉ còn vài ngày trước bầu cử tổng thống.</span></p><p class="Normal" style="margin: 0px 0px 1em; box-sizing: border-box; text-rendering: optimizespeed; line-height: 28.8px;"><span style="background-color: #ffffff;">Thống kê của Reuters cho thấy Mỹ hôm 31/10 ghi nhận thêm 100.233 ca nhiễm nCoV chỉ trong 24 giờ, vượt qua kỷ lục 91.295 trường hợp được ghi nhận một ngày trước đó. Đây cũng là mức tăng cao nhất thế giới, vượt qua con số 97.894 ca trong 24 giờ được ghi nhận tại Ấn Độ hồi giữa tháng 9.</span></p>', 3, 0, 1, NULL, CAST(N'2020-11-01 17:30:28.147' AS DateTime), NULL)
SET IDENTITY_INSERT [dbo].[tbNews] OFF
SET IDENTITY_INSERT [dbo].[tbNhapHang] ON 

INSERT [dbo].[tbNhapHang] ([nhaphang_id], [nhaphang_code], [nhaphang_createdate], [nhaphang_content], [username_id], [hidden]) VALUES (1, N'NH00001', CAST(N'2020-11-13 13:38:38.653' AS DateTime), N'nhaphang_chitiet_soluong', 1, NULL)
INSERT [dbo].[tbNhapHang] ([nhaphang_id], [nhaphang_code], [nhaphang_createdate], [nhaphang_content], [username_id], [hidden]) VALUES (2, N'NH00002', CAST(N'2020-11-13 13:50:01.137' AS DateTime), N'bdashas', 1, NULL)
INSERT [dbo].[tbNhapHang] ([nhaphang_id], [nhaphang_code], [nhaphang_createdate], [nhaphang_content], [username_id], [hidden]) VALUES (3, N'NH00003', CAST(N'2020-11-14 09:46:33.827' AS DateTime), N'nhập hàng', 1, NULL)
INSERT [dbo].[tbNhapHang] ([nhaphang_id], [nhaphang_code], [nhaphang_createdate], [nhaphang_content], [username_id], [hidden]) VALUES (4, N'NH00004', CAST(N'2020-11-14 09:54:07.130' AS DateTime), N'ưerg', 1, NULL)
INSERT [dbo].[tbNhapHang] ([nhaphang_id], [nhaphang_code], [nhaphang_createdate], [nhaphang_content], [username_id], [hidden]) VALUES (5, N'NH00005', CAST(N'2020-11-14 10:22:41.927' AS DateTime), N'nhập hàng', 1, NULL)
SET IDENTITY_INSERT [dbo].[tbNhapHang] OFF
SET IDENTITY_INSERT [dbo].[tbNhapHang_ChiTiet] ON 

INSERT [dbo].[tbNhapHang_ChiTiet] ([nhaphang_chitiet_id], [nhaphang_id], [product_id], [nhaphang_chitiet_soluong], [nhaphang_code], [nhaphang_gianhap], [nhaphang_thanhtien], [username_id]) VALUES (1, 1, 1, 2, N'NH00001', 150000, 300000, 1)
INSERT [dbo].[tbNhapHang_ChiTiet] ([nhaphang_chitiet_id], [nhaphang_id], [product_id], [nhaphang_chitiet_soluong], [nhaphang_code], [nhaphang_gianhap], [nhaphang_thanhtien], [username_id]) VALUES (4, 1, 2, 1, N'NH00001', 200000, 200000, 1)
INSERT [dbo].[tbNhapHang_ChiTiet] ([nhaphang_chitiet_id], [nhaphang_id], [product_id], [nhaphang_chitiet_soluong], [nhaphang_code], [nhaphang_gianhap], [nhaphang_thanhtien], [username_id]) VALUES (9, 3, 2, 2, N'NH00003', 100000, 200000, 1)
INSERT [dbo].[tbNhapHang_ChiTiet] ([nhaphang_chitiet_id], [nhaphang_id], [product_id], [nhaphang_chitiet_soluong], [nhaphang_code], [nhaphang_gianhap], [nhaphang_thanhtien], [username_id]) VALUES (13, 5, 1, 2, N'NH00005', 100000, 200000, 1)
SET IDENTITY_INSERT [dbo].[tbNhapHang_ChiTiet] OFF
SET IDENTITY_INSERT [dbo].[tbProduct] ON 

INSERT [dbo].[tbProduct] ([product_id], [product_position], [product_title], [product_image], [product_summary], [product_content], [product_quantum], [product_show], [product_new], [productcate_id], [title_web], [meta_title], [meta_keywords], [meta_description], [h1_seo], [link_seo], [product_chungloai], [thuonghieu_id], [meta_image], [product_representative], [product_cart], [product_price_new], [product_price], [product_promotions], [product_price_entry], [hidden]) VALUES (1, NULL, N'Sơn móng tay', N'/admin_images/up-img.png', N'đây là tóm tắt mô tả sản phẩm', N'', NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 200000, NULL, NULL, 0)
INSERT [dbo].[tbProduct] ([product_id], [product_position], [product_title], [product_image], [product_summary], [product_content], [product_quantum], [product_show], [product_new], [productcate_id], [title_web], [meta_title], [meta_keywords], [meta_description], [h1_seo], [link_seo], [product_chungloai], [thuonghieu_id], [meta_image], [product_representative], [product_cart], [product_price_new], [product_price], [product_promotions], [product_price_entry], [hidden]) VALUES (2, NULL, N'sản phẩm 2', N'/uploadimages/anh_sanpham/12112020_081847_CH_your kill.png', N'mô tả sản phẩm bvhgc', N'', NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 250000, NULL, NULL, 0)
INSERT [dbo].[tbProduct] ([product_id], [product_position], [product_title], [product_image], [product_summary], [product_content], [product_quantum], [product_show], [product_new], [productcate_id], [title_web], [meta_title], [meta_keywords], [meta_description], [h1_seo], [link_seo], [product_chungloai], [thuonghieu_id], [meta_image], [product_representative], [product_cart], [product_price_new], [product_price], [product_promotions], [product_price_entry], [hidden]) VALUES (3, NULL, N'sản phẩm 1 adba', N'/uploadimages/anh_sanpham/12112020_081652_CH_messager.png', N'dbaddbs', N'', NULL, NULL, NULL, 4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[tbProduct] ([product_id], [product_position], [product_title], [product_image], [product_summary], [product_content], [product_quantum], [product_show], [product_new], [productcate_id], [title_web], [meta_title], [meta_keywords], [meta_description], [h1_seo], [link_seo], [product_chungloai], [thuonghieu_id], [meta_image], [product_representative], [product_cart], [product_price_new], [product_price], [product_promotions], [product_price_entry], [hidden]) VALUES (4, NULL, N'Sơn đỏ ', N'/admin_images/up-img.png', N'Mô tả sản phẩm', N'', NULL, NULL, NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 200000, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[tbProduct] OFF
SET IDENTITY_INSERT [dbo].[tbProductCate] ON 

INSERT [dbo].[tbProductCate] ([productcate_id], [productcate_position], [productcate_title], [productcate_show], [productgroup_id], [title_web], [meta_title], [meta_keywords], [meta_description], [h1_seo], [link_seo], [productcate_parent], [productcate_content], [meta_image], [active], [hidden]) VALUES (3, NULL, N'Sơn móng tay', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[tbProductCate] ([productcate_id], [productcate_position], [productcate_title], [productcate_show], [productgroup_id], [title_web], [meta_title], [meta_keywords], [meta_description], [h1_seo], [link_seo], [productcate_parent], [productcate_content], [meta_image], [active], [hidden]) VALUES (4, NULL, N'Sơn dưỡng', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[tbProductCate] OFF
SET IDENTITY_INSERT [dbo].[tbSlide] ON 

INSERT [dbo].[tbSlide] ([slide_id], [slide_image], [slide_title], [slide_title1], [slide_link], [slide_summary], [slide_content], [hidden]) VALUES (1, N'/uploadimages/anh_slide/14112020_115231_SA_show team.png', NULL, NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[tbSlide] ([slide_id], [slide_image], [slide_title], [slide_title1], [slide_link], [slide_summary], [slide_content], [hidden]) VALUES (2, N'/uploadimages/anh_slide/u5wblhh2.pas.png', NULL, NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[tbSlide] OFF
SET IDENTITY_INSERT [dbo].[tbXuatHang] ON 

INSERT [dbo].[tbXuatHang] ([xuathang_id], [xuathang_code], [xuathang_createdate], [xuathang_content], [username_id], [hidden]) VALUES (2, N'XH00001', CAST(N'2020-11-13 20:00:25.690' AS DateTime), N'Xuất hàng dịch vụ', 1, NULL)
INSERT [dbo].[tbXuatHang] ([xuathang_id], [xuathang_code], [xuathang_createdate], [xuathang_content], [username_id], [hidden]) VALUES (3, N'XH00002', CAST(N'2020-11-14 10:11:58.620' AS DateTime), N'xuất hàng dịch vụ', 1, NULL)
SET IDENTITY_INSERT [dbo].[tbXuatHang] OFF
SET IDENTITY_INSERT [dbo].[tbXuatHang_ChiTiet] ON 

INSERT [dbo].[tbXuatHang_ChiTiet] ([xuathang_chitiet_id], [xuathang_id], [product_id], [xuathang_chitiet_soluong], [username_id]) VALUES (3, 2, 1, 5, 1)
INSERT [dbo].[tbXuatHang_ChiTiet] ([xuathang_chitiet_id], [xuathang_id], [product_id], [xuathang_chitiet_soluong], [username_id]) VALUES (4, 2, 2, 5, 1)
INSERT [dbo].[tbXuatHang_ChiTiet] ([xuathang_chitiet_id], [xuathang_id], [product_id], [xuathang_chitiet_soluong], [username_id]) VALUES (7, 3, 1, 1, 1)
INSERT [dbo].[tbXuatHang_ChiTiet] ([xuathang_chitiet_id], [xuathang_id], [product_id], [xuathang_chitiet_soluong], [username_id]) VALUES (8, 3, 2, 2, 1)
SET IDENTITY_INSERT [dbo].[tbXuatHang_ChiTiet] OFF
SET IDENTITY_INSERT [hel13271_nails].[tbNhomDichVu] ON 

INSERT [hel13271_nails].[tbNhomDichVu] ([dvcate_id], [dvcate_name], [position]) VALUES (1, N'Nails cơ bản', 2)
INSERT [hel13271_nails].[tbNhomDichVu] ([dvcate_id], [dvcate_name], [position]) VALUES (2, N'Trang trí móng', 6)
INSERT [hel13271_nails].[tbNhomDichVu] ([dvcate_id], [dvcate_name], [position]) VALUES (3, N'Gội đầu', 1)
INSERT [hel13271_nails].[tbNhomDichVu] ([dvcate_id], [dvcate_name], [position]) VALUES (4, N'Phụ kiện', 5)
INSERT [hel13271_nails].[tbNhomDichVu] ([dvcate_id], [dvcate_name], [position]) VALUES (5, N'Sơn', 3)
INSERT [hel13271_nails].[tbNhomDichVu] ([dvcate_id], [dvcate_name], [position]) VALUES (6, N'Nối móng', 4)
SET IDENTITY_INSERT [hel13271_nails].[tbNhomDichVu] OFF
ALTER TABLE [dbo].[admin_AccessGroupUserForm]  WITH CHECK ADD FOREIGN KEY([form_id])
REFERENCES [dbo].[admin_Form] ([form_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[admin_AccessGroupUserForm]  WITH CHECK ADD FOREIGN KEY([groupuser_id])
REFERENCES [dbo].[admin_GroupUser] ([groupuser_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[admin_AccessGroupUserModule]  WITH CHECK ADD FOREIGN KEY([groupuser_id])
REFERENCES [dbo].[admin_GroupUser] ([groupuser_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[admin_AccessGroupUserModule]  WITH CHECK ADD FOREIGN KEY([module_id])
REFERENCES [dbo].[admin_Module] ([module_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[admin_AccessUserForm]  WITH CHECK ADD FOREIGN KEY([form_id])
REFERENCES [dbo].[admin_Form] ([form_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[admin_AccessUserForm]  WITH CHECK ADD FOREIGN KEY([username_id])
REFERENCES [dbo].[admin_User] ([username_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[admin_Form]  WITH CHECK ADD FOREIGN KEY([module_id])
REFERENCES [dbo].[admin_Module] ([module_id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[admin_User]  WITH CHECK ADD FOREIGN KEY([groupuser_id])
REFERENCES [dbo].[admin_GroupUser] ([groupuser_id])
ON UPDATE CASCADE
GO
